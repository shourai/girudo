$(document).ready(function(){
   $('.guil-slide-block .job-col').each(function() {
        var boxLink = $(this).find("a.card").attr("href");
        if (boxLink != null) {
            $(this).attr("onClick", "window.open('" + boxLink + "','_top')");
        }
   });
   $('.testimonial').each(function() {
        var boxLink = $(this).find("a.card").attr("href");
        if (boxLink != null) {
            $(this).attr("onClick", "window.open('" + boxLink + "','_top')");
        }
   });
   $('.match-height').matchHeight();

   var num_col = $('.job-col').length;
   if($(window).width() > 1025){
        if(num_col>5){
            $('.job-row').slick({
              centerMode: true,
              centerPadding: '0',
              slidesToShow: 5,
              autoplay:true,
              infinite:true,
              dots: false,
              arrows:true
            });
        }
   }
   if($(window).width() <= 1024){
        $('.job-row').slick({
              slideToScroll:1,
              slidesToShow: 3,
              autoplay:true,
              infinite:true,
              dots: false,
              arrows:false,
              responsive: [
                {
                  breakpoint: 575,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                  }
                }
            ]
        });
   }
});
