<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProjectCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('project_categories')->insert([
        ['name' => 'WEBサイト'],
        ['name' => 'スマホアプリ'],
        ['name' => 'システム開発'],
        ['name' => 'その他']
      ]);

    }
}
