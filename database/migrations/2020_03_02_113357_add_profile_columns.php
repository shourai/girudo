<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProfileColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->text('bio')->nullable();
            $table->string('twitter', 100)->nullable();
            $table->string('github', 100)->nullable();
            $table->string('linkedin', 100)->nullable();
            $table->string('website', 100)->nullable();
            $table->string('profile_pic', 100)->nullable();
            $table->smallInteger('availability')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('bio');
            $table->dropColumn('twitter');
            $table->dropColumn('github');
            $table->dropColumn('linkedin');
            $table->dropColumn('website');
            $table->dropColumn('profile_pic');
            $table->dropColumn('availability');
        });
    }
}
