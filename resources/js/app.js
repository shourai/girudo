/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
var Mustache = require('mustache');

Mustache.tags = ['<%', '%>'];
window.Mustache = Mustache;




require('./home');
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */



const feather = require('feather-icons');
const menuId = document.getElementById('sidebarToggle');
const bodyTag = document.body;

function toggleNav() {
    bodyTag.classList.toggle('sb-sidenav-toggled');
}

document.addEventListener('DOMContentLoaded', function() {
    feather.replace();
    // Toggle the side navigation
    menuId.addEventListener('click', toggleNav);
    /*
    $("#sidebarToggle").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sidenav-toggled");
    });
    */
});
