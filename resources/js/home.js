$(document).ready(function() {
    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){

        $(this).prev(".footer-freelancer").addClass('open-block');
        $(this).prev(".footer-freelancer").find('a').removeClass('btn-black');
        $(this).prev(".footer-freelancer").find('a').text('プロジェクトリストを閉じる -');
        $(this).prev(".footer-freelancer").find('a').addClass('btn-orange');

    }).on('hide.bs.collapse', function(){
        $(this).prev(".footer-freelancer").removeClass('open-block');
        $(this).prev(".footer-freelancer").find('a').addClass('btn-black');
        $(this).prev(".footer-freelancer").find('a').text('プロジェクトリストを表示 +');
        $(this).prev(".footer-freelancer").find('a').removeClass('btn-orange');
    });
});
