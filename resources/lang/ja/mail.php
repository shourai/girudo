<?php

return [
    'Whoops!' => '申し訳ありません',
    'Hello!' => 'こんにちは！',
    'Regards' => '',
    'If you’re having trouble clicking the ":actionText" button, copy and paste the URL below\ninto your web browser: [:displayableActionUrl](:actionURL)' => '":actionText"ボタンをクリックいただけない場合、お手数ですが下記のリンクをブラウザに貼り付けてご確認ください[:displayableActionUrl](:actionURL)',
    'All rights reserved.' => '',
];
