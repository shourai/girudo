@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'home', 'title' => 'グループ', 'text' => ''])
    <section class="container-fluid pt-5">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center flex-column">
                    <h5 class="info-project-title">グループ</h5>

                    @auth
                    <div class="mb-3 mb-sm-0">
                        <a class="btn-common orange text-decoration-none"
                           href="{{ route('group_new') }}">+ グループを設立</a>
                    </div>
                    @endauth

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ $groups->links() }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="grid d-flex justify-content-between" data-columns>
                    @foreach ($groups as $group)
                        <div class="item">
                            <div class="project-content">
                                <p class="home-info-time text-orange">
                                    @if ($group->privacy == '1')
                                        公開
                                    @elseif ($group->privacy == '2')
                                        招待制
                                    @endif
                                </p>
                                <div class="info-description text-break">{{ $group->getDescription() }}</div>
                            </div>

                            <div
                                class="main-project-footer d-flex flex-column justify-content-center align-items-center">
                                <h6><a class="project-name"
                                       href="{{ route('group', ['group_id' => $group->id]) }}">{{ $group->name }}</a>
                                </h6>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ $groups->links() }}
            </div>
        </div>

    </section>
@endsection
