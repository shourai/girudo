@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'logo', 'title' => 'グループ編集'])

    <div class="container pt-5">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="card sb-card-header-actions mx-auto">
            <div class="card-header">
                グループ編集
                <button type="button" class="btn-common btn-close" onclick="event.preventDefault(); if (!confirm('本当にグループを削除しますか？')) { return false; } document.getElementById('delete-group-form').submit();">
                    @include('components.icons.useSprite', ['name' => 'cross'])
                </button>
            </div>
            <form method="post" action="{{ route('group_edit', ['group_id' => $group->id]) }}" enctype="multipart/form-data">
                <div class="card-body pb-3">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name" class="font-weight-bold">グループ名*</label>
                        <input name="name" class="form-control @error('name') is-invalid @enderror" id="name" type="text" required value="{{ old('name') ?? $group->name }}">
                    </div>

                    <div class="form-group">
                        <label for="description" class="font-weight-bold">紹介文</label>
                        <textarea rows="5" id="description" name="description" required class="form-control @error('description') is-invalid @enderror">{{ old('description') ?? $group->description }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="privacy" class="font-weight-bold">公開設定*</label>
                        <select class="form-control @error('privacy') is-invalid @enderror" required id="privacy" name="privacy">
                            <option value="1" {{ (old('privacy') ?? $group->privacy) == '1' ? 'selected' : '' }}>公開</option>
                            <option value="2" {{ (old('privacy') ?? $group->privacy) == '2' ? 'selected' : '' }}>招待制</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('group', ['group_id' => $group->id]) }}" class="btn-common cancel mr-3 text-decoration-none">
                        キャンセル
                    </a>
                    <button type="submit" class="btn-common orange float-right">
                        保存
                    </button>
                </div>
            </form>
        </div>
    </div>

<form method="post" id="delete-group-form" action="{{ route('group_delete', ['group_id' => $group->id]) }}" style="deplay: none;">
    @csrf
</form>
@endsection
