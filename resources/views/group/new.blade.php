@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'logo', 'title' => 'プロジェクト編集'])

    <div class="container pt-5">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="card sb-card-header-actions mx-auto">
            <div class="card-header">
                グループ
            </div>
            <form method="post" action="{{ route('group_new') }}" enctype="multipart/form-data">
                <div class="card-body pb-5">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name" class="font-weight-bold">グループ名*</label>
                        <input name="name" class="form-control @error('name') is-invalid @enderror" id="name" type="text" required value="{{ old('name') ?? '' }}">
                    </div>

                    <div class="form-group">
                        <label for="description" class="font-weight-bold">紹介文</label>
                        <textarea rows="5" id="description" name="description" required class="form-control @error('description') is-invalid @enderror">{{ old('description') ?? '' }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="privacy" class="font-weight-bold">公開設定*</label>
                        <select class="form-control @error('privacy') is-invalid @enderror" required id="privacy" name="privacy">
                            <option value="1" {{ old('privacy')  == '1' ? 'selected' : '' }}>公開</option>
                            <option value="2" {{ old('privacy')  == '2' ? 'selected' : '' }}>招待制</option>
                        </select>
                    </div>

                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('groups') }}" class="btn-common cancel mr-3 text-decoration-none">
                        キャンセル
                    </a>
                    <button type="submit" class="btn-common orange float-right">
                        保存
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
