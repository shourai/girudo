@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'logo', 'title' => $group->name, 'text' => ($group->privacy == '1') ? '公開グループ' : '招待制グループ'])

    <div class="container-fluid">
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        <div class="row pt-5">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center">
                    <h5 class="info-project-title">グループ説明</h5>
                    @auth()
                        @if ($group->isAdmin(Auth::id()))
                            <div class="mr-0 mr-sm-4">
                                <a class="btn-common orange text-decoration-none" href="{{ route('group_edit', ['group_id' => $group->id]) }}">編集</a>
                            </div>
                        @endif
                    @endauth
                </div>

                <div class="info-block main-project-info mx-0 mx-sm-4">
                    <div class="info-body px-3 px-sm-4 pt-4">
                        <p class="info-months text-orange">
                            @if ($group->privacy == '1')
                                公開
                            @elseif ($group->privacy == '2')
                                招待制
                            @endif
                        </p>
                        <div class="info-description">{{ $group->getDescription() }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row pt-5">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center">
                    <h5 class="info-project-title">プロジェクト一覧</h5>
                    @auth()
                    @if ($group->isAdmin(Auth::id()))
                            <div class="mr-0 mr-sm-4">
                                <a class="btn-common orange text-decoration-none text-nowrap" href="{{ route('new_project') }}">追加</a>
                            </div>
                        @endif
                    @endauth
                </div>
                <div class="info-block-user main-project-info mx-0 mx-sm-4">
                    <ul class="pl-0">
                        @forelse ($group->projects as $project)
                            <li class="project-link">
                                <a href="{{ route('project', ['project_id' => $project->id]) }}">{{ $project->name }}</a>
                                @if ($group->isAdmin(Auth::id()))
                                    <span>
                                        <a class="edit-btn"
                                           href="{{ route('edit_project', ['project_id' => $project->id]) }}"><i
                                                data-feather="edit" class="align-middle"></i></a>
                                    </span>
                                @endif
                            </li>
                        @empty
                            <p>なし</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-color-gray pb-5 mt-3 mt-sm-5">
        <div class="d-flex flex-sm-row justify-content-between align-items-center">
            <h5 class="info-project-title">参加メンバー</h5>
            @auth()
                @if ($group->isAdmin(Auth::id()))
                    <div class="mr-0 mr-sm-4">
                        <a class="btn-common orange text-decoration-none" href="#add-member">招待</a>
                    </div>
                @endif
            @endauth
        </div>
        <div class="info-block info-block-members mx-0 mx-sm-4">
                @forelse ($group->members as $member)
                    <div class="info-member mb-3">
                            <div class="member-title d-flex flex-sm-row justify-content-between align-items-center flex-column">
                                <h5><a href="{{ route('profile', ['user_id' => $member->id]) }}">{{ $member->name }}</a></h5>
                                <div class="d-flex">
                                    @auth
                                        @if ($member->id != Auth::id())
                                            <div class="text-right mb-2">
                                                <a href="{{ route('user_message', ['user_id' => $member->id, 'initiator_id' => Auth::id()]) }}" class="btn-common mail text-decoration-none">@include('components.icons.useSprite', ['name' => 'mail']) </a>
                                            </div>
                                        @endif
                                    @endauth
                                </div>

                            </div>

                            <div style="background-image: url('{{ $member->profile_pic ? '/images/'.$member->profile_pic :  asset('/images/user.png') }}')"
                                 class="member-img"></div>
                            <div class="info-skills d-flex justify-content-center flex-wrap">
                                @foreach ($member->skills as $skill)
                                    <div class="info-skill-home">{{ $skill->name }}</div>
                                @endforeach
                            </div>
                            @auth
                                @if ($group->isAdmin(Auth::id()) && $member->id != Auth::id())
                                <div class="position-absolute">
                                    <form method="post" action="{{ route('delete_group_member', ['group_id' => $group->id]) }}">
                                        @csrf
                                        <input type="hidden" name="member_id" value="{{ $member->id }}" />
                                        <button onclick="if (!confirm('本当にメンバーから削除しますか？')) return false;" class="btn-common btn-close btn-remove-member">@include('components.icons.useSprite', ['name' => 'cross'])</a>
                                    </form>
                                </div>
                                @endif
                            @endauth
                    </div>
                @empty
                    <p class="ml-2">なし</p>
                @endforelse

        </div>
    </div>

    @auth()
        @if($group->isAdmin(Auth::id()))
            <div class="container-fluid bg-color-orange" id="add-member">
                <div class="p-sm-5 p-2">
                    <div class="add-member-form main-project-info">
                        <div class="form-main form-title">メンバー追加</div>
                        <div>
                            <form method="post" action="{{ route('add_group_member', ['group_id' => $group->id]) }}">
                                @csrf
                                <div class="form-row form-input">
                                    <div class="col-12">
                                        <div class="input-group">
                                            <input type="text" name="member" class="form-control" placeholder="名前を入力してください">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-main">
                                    <div class="input-group-append d-flex justify-content-end">
                                        <button class="btn-common outline-orange">メンバー追加</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        @endif
    @endauth

@endsection
