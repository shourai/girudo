@extends('layouts.landing')

@section('content')
<header class="page-header page-header-dark bg-img-cover overlay guil-page-header" style="background-image: url('{{ asset('images/computer-monitor.jpg') }}')">
    <div class="page-header-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <h1>フリーランスエンジニアのためのクラウドソーシング</h1>
                    <!-- <h2>ギルドー</h2> -->
                </div>
            </div>
        </div>
    </div>
</header>
<section class="bg-white guil-padding guil-two-col">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="guil-left-image">
                    <img src="{{ asset('images/guildoh-sprout.png') }}" class="img-fluid">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="guil-twocol-detail">
                    <h3>フリーランスエンジニアに特化したクラウドソーシング</h3>
                    <p>フリーランス1年目の廃業率は３０％〜４０％と言われています。<br>
                        フリーランスエンジニアも例外ではありません。<br>
                        私たちは、そんなフリーランスエンジニアのためにクラウドソージングを行います。<br><br>
                        こんなお悩みはありませんか？
                    </p>
                    <ul>
                        <li>フリーランスになったはいいが、仕事が見つからない</li>
                        <li>現在案件で忙しいのでエンジニアを探している</li>
                        <li>単価が低く、フリーランスとして仕事を続けられない。</li>
                    </ul>
                    <a href="{{ route('unable_register') }}" class="orange-link with-arrow">会員登録（無料）</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-ash guil-two-col">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="guil-twocol-detail">
                    <h3>プロジェクトとリソースが空いているエンジニアが探せる</h3>

                    <p>ギルドーでは2つのことができます。</p>
                    <ul>
                        <li>システム開発やアプリ開発を始め、<br>WordPressなどのウェブサイト開発など様々なプロジェクトを探すことができます。</li><br>
                        <li>現在リソースが空いているエンジニアが即座に探せます。<br>今忙しくて人手が欲しい人はぜひメッセージを送ってみましょう。</li>
                    </ul>
                    <a href="{{ route('unable_register') }}" class="orange-link with-arrow">会員登録（無料）</a>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="guil-right-image">
                    <img src="{{ asset('images/iMac.png') }}" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="guil-three-col guil-padding text-center">
    <div class="container">
        <div class="heading">
            <h2 class="h1">ギルドーのメリット</h2>
            <a href="{{ route('unable_register') }}" class="orange-link with-arrow">会員登録（無料）</a>
        </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="guil-threecol-detail">
                    <div class="match-height">
                        <img src="{{ asset('images/Group-7.png') }}" class="img-fluid">
                    </div>
                    <h3>新しい案件が見つかる</h3>
                    <p>システム開発・ウェブサイト開発など様々な案件が探せます</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="guil-threecol-detail">
                    <div class="match-height">
                        <img src="{{ asset('images/Group-11.png') }}" class="img-fluid">
                    </div>
                    <h3>エンジニア不足解消</h3>
                    <p>エンジニアのリソース状況が一目瞭然です。メッセージを送ってみましょう</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="guil-threecol-detail">
                    <div class="match-height">
                        <img src="{{ asset('images/Group-15.png') }}" class="img-fluid">
                    </div>
                    <h3>工数価格の担保できる案件</h3>
                    <p>登録できる案件は時給1000円以上のみで、フリーランスとして仕事が続くようサポートします。</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="guil-slide-block guil-padding">
    <div class="heading">
        <h2 class="h1">現在の注目プロジェクト</h2>
        <!-- <h2>志を同じくするフリーランサーとコラボレーションする</h2> -->
        <a href="{{ route('unable_register') }}" class="orange-link with-arrow">会員登録（無料）</a>
    </div>
    <div class="job-row">
        @foreach ($recruiting_projects as $project)
            <div class="job-col">
                {{-- <a href="{{ route('project', ['project_id' => $project->id]) }}" class="card lift d-none">  </a> --}}
                <div class="guil-job-block">
                    <div class="top-part">
                        <div class="price-block">
                            <div class="d-flex flex-wrap justify-content-between">
                            @if ($project->budget_hourly)
                                <p>{{ number_format($project->budget_hourly) }}<span>円/時間</span></p>
                            @endif
                            @if ($project->budget_total)
                                <p>{{ number_format($project->budget_total) }}<span>円/予算</span></p>
                            @endif
                            </div>

                            @if ($project->project_length == '1')
                                <h6>1ヶ月以内</h6>
                            @elseif ($project->project_length == '2')
                                <h6>1〜3ヶ月</h6>
                            @elseif ($project->project_length == '3')
                                <h6>3〜6ヶ月</h6>
                            @else
                               <h6>半年以上</h6>
                            @endif
                        </div>
                        <div style="background-image: url('{{ asset('images/Base-1.png') }}')" class="card-img">
                            <div class="center-detail">
                                {{ $project->getDescription() }}
                            </div>
                        </div>
                        <div style="background-image: url('{{ asset('images/Base.png') }}')" class="card-img active-img">
                            <div class="center-detail">
                                {{ $project->getDescription() }}
                            </div>
                        </div>
                        <div class="button-block">
                            @foreach ($project->skills as $skill)
                                <button class="btn button2">{{ $skill->name }}</button>
                            @endforeach
                       </div>
                    </div>
                    <div class="bottom-part">
                        <p>{{ $project->name }}</p>
                         @if ($project->category_id)
                            <h6>カテゴリー: {{ $project->category->name }}</h6>
                        @endif
                        {{--
                        <div class="read-more">
                            <a href="#">もっと見る</a>
                        </div>
                        --}}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>
<!-- <section class="guil-three-col guil-padding text-center">
    <div class="container">
        <div class="heading">
            <h2 class="h1">ハイスキルなフリーランサーが活躍しています。</h2>
            <a href="{{ route('unable_register') }}" class="orange-link with-arrow">会員登録（無料）</a>
        </div>
        <div class="row">
            @foreach ($available_freelancers as $freelancer)
            <div class="col-12 col-md-4">
                <div class="guil-threecol-detail mb-5 testimonial">
                    <a href="{{ route('profile', ['user_id' => $freelancer->id]) }}" class="card lift d-none">
                    </a>
                    <div class="match-height">
                        <div class="round-image">
                            <img class="card-img-top round mx-auto" src="{{ $freelancer->profile_pic ? '/images/'.$freelancer->profile_pic : 'https://source.unsplash.com/SYTO3xs06fU/300x300' }}" />
                        </div>
                    </div>
                    <h5>{{ $freelancer->name }}</h5>
                    @if ($loop->first)
                        <h6>プロジェクトマネージャー・エンジニア</h6>
                        <h4>"開発やディレクション、PM、CTOなど様々な経験のある方です。 介護事業所向けのSaaSを企画段階から開発、リリースまで一人で1ヶ月半ほどで行っており、実装スピードの速さを強みにしています。"</h4>
                    @elseif ($loop->last)
                        <h6>Webデザイナー・アートディレクター</h6>
                        <h4>"グラフィックデザイナーとして約5年間広告代理店系制作会社に勤務。広告の企画からデザイン、ディレクションまで行い、その後、独立しクリエイティブディレクターとして幅広く活躍しています。"</h4>
                    @else
                        <h6>iOSエンジニア・Androidエンジニア</h6>
                        <h4>"大手通信企業にて新規サービスの開発やプロジェクトリーダーを経験された後、ニッチな情報を扱うベンチャー企業にてエンジニア第一号として入社、スマホアプリの開発に携わっていました。"</h4>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section> -->
<!-- <section class="bg-light pt-10 pb-15">
    <div class="container mb-10">
       <div class="heading">
            <h2 class="h1">Current featured jobs</h2>
            <h2>Collaborate with like-minded freelancers</h2>
            <a href="#" class="orange-link">Sign up free </a>
        </div>
        <div class="row">
            @foreach ($recruiting_projects as $project)
            <div class="col-lg-4 my-5 mb-lg-0">
                <a href="{{ route('project', ['project_id' => $project->id]) }}" class="card lift">
                    <img class="card-img" src="https://source.unsplash.com/SYTO3xs06fU/300x200" />
                    <div class="card-img-overlay" style="background-color: rgba(0, 0, 0, 0.6);">
                        <h4 class="card-title text-white">{{ $project->name }}</h4>
                        @if ($project->category_id)
                            <p class="card-text text-white">カテゴリー: {{ $project->category->name }}</p>
                        @endif
                    </div>
                    <div class="card-body text-center py-3" style="z-index:1; background-color: #fff">
                        <pre class="small my-2">{{ $project->getDescription() }}</pre>
                        <div class="small">
                            @foreach ($project->skills as $skill)
                            <span class="badge badge-pill badge-primary">{{ $skill->name }}</span>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer text-center text-xs" style="z-index:1; background-color: rgb(247, 247, 247)">
                        @if ($project->budget_hourly)
                        <span class="font-weight-bold">{{ number_format($project->budget_hourly) }}</span>円 / 時間
                        @else
                        <span class="font-weight-bold">{{ number_format($project->budget_total) }}</span>円
                        @endif
                        <div class="small">
                            @if ($project->project_length == '1')
                                1か月以内
                            @elseif ($project->project_length == '2')
                                1から3か月
                            @elseif ($project->project_length == '3')
                                3から6か月
                            @else
                                半年以上
                            @endif
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section> -->
<!-- <section class="bg-white py-10">
    <div class="container mb-10">
        <div class="card mt-n15 mb-10 z-1">
            <div class="card-body p-5">
                <div class="row align-items-center justify-content-md-center">
                    <div class="col-lg-12">
                        <h4>仮登録していただいた方にはサービス開始時にご連絡します。</h4>
                        <p class="lead mb-1">まずは下記フォームよりお気軽にお問い合わせください。<p>
                        <p class="lead mb-1">ご登録いただいた連絡先に、サービス開始時に本登録のご案内をお送りします。
                        <p class="lead mb-4">サービス開始予定：2020年　夏</p>
                    </div>
                    <div class="col-auto">
                        <a href="./register" class="btn btn-warning mx-auto btn-lg">仮登録する</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="mb-5 text-center">
                    <h1 class="text-uppercase-expanded text-primary mb-2">ソースに余裕がある</h1>
                    <p class="lead mb-0">あなたのプロジェクトを手伝ってくれる人たち</p>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($available_freelancers as $freelancer)
            <div class="col-lg-4 my-5 mb-lg-0">
                <a href="{{ route('profile', ['user_id' => $freelancer->id]) }}" class="card lift">
                    <img class="card-img-top" src="{{ $freelancer->profile_pic ? '/images/'.$freelancer->profile_pic : 'https://source.unsplash.com/SYTO3xs06fU/300x300' }}" />
                    <div class="card-body text-center py-3">
                        <h6 class="card-title mb-0">{{ $freelancer->name }}</h6>
                    </div>
                    @if ($freelancer->skills)
                    <div class="card-footer">
                        <div class="small mb-2">
                            @foreach ($freelancer->skills as $skill)
                                <span class="badge badge-pill badge-primary">{{ $skill->name }}</span>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section> -->
<section class="guil-banner-block" >
    <div style="background-image: url('{{ asset('images/bannerimg.jpg') }}')" class="guil-banner-img"></div>
    <div class="container">
        <div class="banner-detail">
            <h2 class="h1 text-center">今からギルドーを始める！！</h2>
            <a href="{{ route('unable_register') }}"><button class="btn btn-white">会員登録（無料）</button></a>
        </div>
    </div>
</section>
@endsection
