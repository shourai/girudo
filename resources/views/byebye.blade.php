@extends('layouts.app')

@section('content')
@include('components.content_top_block', ['icon' => 'user', 'title' => 'プロフィール', 'test' => '削除' ])
<div class="container-fluid pt-5">
    <div class="container">
        <div class="card">
            <div class="card-header">プロフィールが削除されました</div>
            <div class="card-body">
                <p>プロフィールの削除処理が完了しました。またのご利用をお待ちしております。</p>
            </div>
            <div class="card-footer">
                <a href="{{ url('/') }}" class="btn-common orange float-right">
                    トップへ戻る
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
