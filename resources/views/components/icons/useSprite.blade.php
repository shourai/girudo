@if (isset($name))
    <svg class="sprite-svg">
        <use xlink:href="#{{ $name }}"></use>
    </svg>
@endif
