<div class="row mx-0">
    @if(isset($user_messages))
    <div class="col-12 col-sm-auto chat-list px-0">
        @foreach ($user_messages as $message)
            @if(isset($project))
            @if (Auth::id() == $project->owner_id)
            <a
                href="{{ route('project', ['project_id' => $message->project_id]) }}?initiator_id={{ $message->initiator_id }}"
                class="btn chat-list-card {{ $project->id == $message->project_id && $initiator_id == $message->initiator_id ? 'active' : '' }}">
                @if (Auth::id() == $message->initiator->id)
                <h4>{{ $message->project->owner->name }}</h4>
                @else
                <h4>{{ $message->initiator->name }}</h4>
                @endif
                <p>{{ $message->message }}</p>
            </a>
            @endif
            @else
            <a
                href="{{ route('user_message', ['user_id' => $message->user_id_smaller, 'initiator_id' => $message->user_id_larger]) }}"
                class="btn chat-list-card {{ ($message->user_id_smaller == $user_id && $message->user_id_larger == $initiator_id) || ($message->user_id_smaller == $initiator_id && $message->user_id_larger == $user_id) ? 'active' : '' }}"
            >
                @if ($message->userSmaller->id == Auth::id())
                    <h4>{{ $message->userLarger->name }}</h4>
                @else
                    <h4>{{ $message->userSmaller->name }}</h4>
                @endif
                <p>{{ $message->message }}</p>
            </a>
            @endif
        @endforeach
    </div>
    @endif

    <div class="col flex-grow-1 d-flex flex-column px-0 px-sm-2">
        <div class="chat-messages-container">

            <div class="flex-grow-1 chat-message-list" id="messageBox">
                @forelse ($messages as $message)
                    @if($message->send_id == Auth::id())
                        <div class="user-message-container">
                            <div class="user-text-container right">
                                <div class="user-text">{{ $message->message }}</div>
                                <div class="user-text-time">{{ $message->created_at }}</div>
                            </div>
                            <div class="user-message-avatar">
                                @if($message->author->profile_pic)
                                    <img src="/images/{{ $message->author->profile_pic }}"/>
                                @else
                                    @include('components.icons.useSprite', ['name' => 'logo'])
                                @endif
                                <span class="user-name">{{ $message->author->name }}</span>
                            </div>
                        </div>
                    @else
                        <div class="user-message-container">
                            <div class="user-message-avatar">
                                @if($message->author->profile_pic)
                                    <img src="/images/{{ $message->author->profile_pic }}"/>
                                @else
                                    @include('components.icons.useSprite', ['name' => 'logo'])
                                @endif
                                <span class="user-name">{{ $message->author->name }}</span>
                            </div>
                            <div class="user-text-container left">
                                <div class="user-text">{{ $message->message }}</div>
                                <div class="user-text-time">{{ $message->created_at }}</div>
                            </div>
                        </div>
                    @endif
                @empty
                    {{--<p class="text-white">プロジェクト担当者に連絡しましょう</p>--}}
                @endforelse
            </div>

            <div class="chat-divider"></div>

            <div class="chat-send-form-container">
                <div class="">
                    <form
                        method="POST"
                        action="{{ isset($project) ? route('project_message', ['project_id' => $project->id, 'initiator_id' => $initiator_id]) : route('user_message', ['user_id' => $user_id, 'initiator_id' => $initiator_id]) }}"
                    >
                        {{ csrf_field() }}
                        <div class="form-group mb-4">
                            <textarea name="message" class="chat-textarea" rows="5" placeholder="メッセージを送りましょう"></textarea>
                        </div>
                        <div class="text-right">
                            <button class="btn-common mail">
                                @include('components.icons.useSprite', ['name' => 'mail'])
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        var box = document.getElementById('messageBox');
        box.scrollTop = box.scrollHeight;
    });
</script>
