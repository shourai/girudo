<div class="content-top">
    <div class="content-top-image"></div>
    <div class="content-top-text d-flex justify-content-between align-items-center flex-wrap">
        <div>
        @if (isset($title))
            <h1>
                @include('components.icons.useSprite', ['name' => isset($icon) ? $icon : null])
                {{ $title }}
            </h1>
        @endif
        @if (isset($text))
            <p>{{ $text }}</p>
        @endif
        </div>
        <div>
            @if(isset($status))
                @if ($status === 1)
                    <a href="{{ route('edit_profile', ['user_id' => $user->id]) }}#availability" class="btn-common status-green">リソース余裕あり</a>
                @else
                    <a href="{{ route('edit_profile', ['user_id' => $user->id]) }}#availability" class="btn-common status-yellow">リソース余裕なし</a>
                @endif
            @endif
        </div>
    </div>
</div>
