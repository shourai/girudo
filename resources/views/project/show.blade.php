@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'logo', 'title' => $project->name, 'text' => $project->category->name])

    <div class="container-fluid">
        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        <div class="row pt-5">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center flex-column">
                    <h5 class="info-project-title">プロジェクト説明</h5>
                    @auth()
                        @if ($project->owner_id == Auth::id())
                            <div class="mr-0 mr-sm-4">
                                <a class="btn-common orange text-decoration-none" href="{{ route('edit_project', ['project_id' => $project->id]) }}">編集</a>
                            </div>
                        @endif
                    @endauth
                </div>

                <div class="info-block main-project-info mx-0 mx-sm-4">

                    <div class="d-flex flex-wrap justify-content-between">
                    @if ($project->budget_hourly)
                        <p class="info-hour mb-1 px-3 px-sm-4">{{ number_format($project->budget_hourly) }}円/最低時給</p>
                    @endif
                    @if ($project->budget_total)
                        <p class="info-hour mb-1 px-3 px-sm-4">{{ number_format($project->budget_total) }}円/予算</p>
                    @endif
                    </div>

                    <div class="info-body px-3 px-sm-4">
                        <div class="d-flex flex-wrap justify-content-between">
                        @if ($project->project_status == '1')
                            <p class="info-months">
                                @if ($project->project_length == '1')
                                    1ヶ月以内
                                @elseif ($project->project_length == '2')
                                    1〜3ヶ月
                                @elseif ($project->project_length == '3')
                                    3〜6ヶ月
                                @else
                                    半年以上
                                @endif
                            </p>
                        @endif
                        @if ($project->group)
                            <p class="info-months">
                                @if ($project->group->privacy == 1)
                                    <a href="{{ route('group', ['group_id' => $project->group->id]) }}">{{ $project->group->name }}</a>
                                @else
                                    プライベートグループ
                                @endif
                            </p>
                        @endif
                        </div>

                        <div class="info-description">{{ $project->getDescription() }}</div>
                        @if ($project->skills)
                        <div class="info-skills d-flex justify-content-center flex-wrap">
                            @foreach ($project->skills as $skill)
                                <span class="info-skill">{{ $skill->name }}</span>
                            @endforeach
                        </div>
                            @endif
                    </div>
                        @auth()
                            @if(Auth::id() != $project->owner_id)
                                <div class="info-footer d-flex justify-content-end">
                                    <a href="#project-chat" class="btn-common mail-text text-decoration-none"> @include('components.icons.useSprite', ['name' => 'mail']) メッセージ</a>
                                </div>
                            @endif
                        @endauth
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-color-gray pb-5 mt-3 mt-sm-5">
        <div class="d-flex flex-sm-row justify-content-between align-items-center flex-column">
            <h5 class="info-project-title">参加メンバー</h5>
            @auth()
                @if ($project->owner_id == Auth::id())
                    <div class="mr-0 mr-sm-4">
                        <a class="btn-common orange text-decoration-none" href="#add-member">追加</a>
                    </div>
                @endif
            @endauth
        </div>
        <div class="info-block info-block-members mx-0 mx-sm-4">
                @forelse ($project->members as $member)
                    <div class="info-member mb-3">
                            <div class="member-title d-flex flex-sm-row justify-content-between align-items-center flex-column">
                                <h5><a href="{{ route('profile', ['user_id' => $member->id]) }}">{{ $member->name }}</a></h5>
                                <div class="d-flex">
                                    @auth
                                        @if ($member->id != Auth::id())
                                            <div class="text-right mb-2">
                                                <a href="{{ route('user_message', ['user_id' => $member->id, 'initiator_id' => Auth::id()]) }}" class="btn-common mail text-decoration-none">@include('components.icons.useSprite', ['name' => 'mail']) </a>
                                            </div>
                                        @endif
                                    @endauth
                                </div>

                            </div>

                            <div style="background-image: url('{{ $member->profile_pic ? '/images/'.$member->profile_pic :  asset('/images/user.png') }}')"
                                 class="member-img"></div>
                            <div class="info-skills d-flex justify-content-center flex-wrap">
                                @foreach ($member->skills as $skill)
                                    <div class="info-skill-home">{{ $skill->name }}</div>
                                @endforeach
                            </div>
                            @auth
                                @if ($project->owner_id == Auth::id() && $member->id != Auth::id())
                                <div class="position-absolute">
                                    <form method="post" action="{{ route('delete_project_member', ['project_id' => $project->id]) }}">
                                        @csrf
                                        <input type="hidden" name="member_id" value="{{ $member->id }}" />
                                        <button onclick="if (!confirm('本当にメンバーから削除しますか？')) return false;" class="btn-common btn-close btn-remove-member">@include('components.icons.useSprite', ['name' => 'cross'])</a>
                                    </form>
                                </div>
                                @endif
                            @endauth
                    </div>
                @empty
                    <p class="ml-2">なし</p>
                @endforelse

        </div>
    </div>

    @auth()
        @if(Auth::id() == $project->owner_id)
            <div class="container-fluid bg-color-orange" id="add-member">
                <div class="p-sm-5 p-2">
                    <div class="add-member-form main-project-info">
                        <div class="form-main form-title">メンバー追加</div>
                        <div>
                            <form method="post" action="{{ route('add_project_member', ['project_id' => $project->id]) }}">
                                @csrf
                                <div class="form-row form-input">
                                    <div class="col-12">
                                        <div class="input-group">
                                            <input type="text" name="member" class="form-control" placeholder="名前を入力してください">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-main">
                                    <div class="input-group-append d-flex justify-content-end">
                                        <button class="btn-common outline-orange">メンバー追加</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        @endif
    @endauth

    {{--            <div class="card">--}}
    {{--                <div class="card-header">参加メンバー</div>--}}
    {{--                <div class="card-body">--}}
    {{--                    @foreach ($project->members as $member)--}}
    {{--                    <div class="card mb-3">--}}
    {{--                        <div class="row no-gutters">--}}
    {{--                            <div class="col-md-3"><img src="{{ $member->profile_pic ? '/images/'.$member->profile_pic : 'https://source.unsplash.com/QAB-WJcbgJk/256x256' }}" width="100%"></div>--}}
    {{--                            <div class="col-md-9">--}}
    {{--                                <div class="card-body">--}}
    {{--                                    <h5 class="card-title"><a href="{{ route('profile', ['user_id' => $member->id]) }}">{{ $member->name }}</a></h5>--}}
    {{--                                    <div class="card-text">--}}
    {{--                                        @foreach ($member->skills as $skill)--}}
    {{--                                            <span class="badge badge-pill badge-primary">{{ $skill->name }}</span>--}}
    {{--                                        @endforeach--}}
    {{--                                        @auth--}}
    {{--                                        @if ($member->id != Auth::id())--}}
    {{--                                            <div class="text-right mb-2">--}}
    {{--                                                <a href="{{ route('user_message', ['user_id' => $member->id, 'initiator_id' => Auth::id()]) }}" class="btn btn-lg btn-secondary"><i data-feather="send"></i></a>--}}
    {{--                                            </div>--}}
    {{--                                        @endif--}}
    {{--                                        @endauth--}}
    {{--                                        @if ($project->owner_id == Auth::id() && $member->id != Auth::id())--}}
    {{--                                        <div class="text-right">--}}
    {{--                                            <form method="post" action="{{ route('delete_project_member', ['project_id' => $project->id]) }}">--}}
    {{--                                                @csrf--}}
    {{--                                                <input type="hidden" name="member_id" value="{{ $member->id }}" />--}}
    {{--                                                <button onclick="if (!confirm('本当にメンバーから削除しますか？')) return false;" class="btn btn-lg btn-danger"><i data-feather="trash"></i></a>--}}
    {{--                                            </form>--}}
    {{--                                        </div>--}}
    {{--                                        @endif--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    @endforeach--}}
    {{--                </div>--}}
    {{--            </div>--}}




    @auth
    @if($initiator_id)
        <div class="project-chat" id="project-chat">
            @include('components.chat', [
                'user_messages' => $project_messages,
                'messages' => $messages,
                'user_id' => null,
                'initiator_id' => $initiator_id,
                'project' => $project
            ])
        </div>
    @endif
    @endauth

@endsection
