<?php $isInCreateProject = true; ?>

@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'logo', 'title' => '新規プロジェクト'])

    <div class="container-fluid pt-5 px-md-10">
        <div class="card">
            <div class="card-header nav-wizard-tab">
                <div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                    <a class="nav-item nav-link active">
                        <div class="wizard-step-icon">1</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">基本情報</div>
                            <div class="wizard-step-text-details">プロジェクト名と説明</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link nav-wizard-next">
                        <div class="wizard-step-icon">2</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">必要なスキル</div>
                            <div class="wizard-step-text-details">スキルに関する情報</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link nav-wizard-next">
                        <div class="wizard-step-icon">3</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">予算など</div>
                            <div class="wizard-step-text-details">予算や時期</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="progress wizard-bar">
                <div class="progress-bar wizard-17" role="progressbar" style="width: 17%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-none" role="progressbar" style="width: 83%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <form method="post" action="{{ route('project_setup_step2') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                @if (isset($skill))
                @foreach ($skill as $i => $s)
                <input type="hidden" name="skill[]" value="{{ $s }}" />
                @endforeach
                @endif
                <div class="card-body mt-4">
                    <h3 class="wizard-title">ステップ１</h3>
                    <h5 class="wizard-sub-title">プロジェクトの基本情報</h5>

                    <div class="form-group">
                        <label for="name" class="font-weight-bold">プロジェクト名*</label>
                        <input name="name" class="form-control @error('name') is-invalid @enderror" id="name" required type="text" value="{{ old('name') ?? $name ?? null }}">
                    </div>

                    <div class="form-group">
                        <label for="category" class="font-weight-bold">カテゴリー</label>
                        <select class="form-control @error('category') is-invalid @enderror" id="category" name="category">
                            @foreach ($categories as $cat)
                                <option value="{{ $cat->id }}" {{ (old('category') ?? $category ?? null) == $cat->id ? 'selected="selected"' : '' }}>{{ $cat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="description" class="font-weight-bold">説明</label>
                        <textarea rows="5" id="description" name="description" required class="form-control @error('description') is-invalid @enderror">{{ old('description') ?? $description ?? null }}</textarea>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn-common orange float-right">
                        次へ
                    </button>
                </div>
	            <div class="card-header nav-wizard-tab nav-wizard-xl">
                	<div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                    	<a class="nav-item nav-link">
                            <div class="wizard-step-icon">2</div>
                            <div class="wizard-step-text">
                                <div class="wizard-step-text-name">必要なスキル</div>
                                <div class="wizard-step-text-details">スキルに関する情報</div>
                            </div>
                        </a>
                        <a class="nav-item nav-link">
                            <div class="wizard-step-icon">3</div>
                            <div class="wizard-step-text">
                                <div class="wizard-step-text-name">予算など</div>
                                <div class="wizard-step-text-details">予算や時期</div>
                            </div>
                        </a>
                    </div>
	            </div>
            </form>
        </div>
    </div>
@endsection
