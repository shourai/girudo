<?php $isInCreateProject = true; ?>

@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'logo', 'title' => '新規プロジェクト'])

    <div class="container-fluid pt-5 px-md-10">
        <div class="card">
            <div class="card-header nav-wizard-tab">
                <div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                    <a class="nav-item nav-link" href="#" onclick="event.preventDefault(); document.getElementById('step1-form').submit();">
                        <div class="wizard-step-icon">1</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">基本情報</div>
                            <div class="wizard-step-text-details">プロジェクト名と説明</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link" href="#" onclick="event.preventDefault(); document.getElementById('prev-form').submit();">
                        <div class="wizard-step-icon">2</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">必要なスキル</div>
                            <div class="wizard-step-text-details">スキルに関する情報</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link active">
                        <div class="wizard-step-icon">3</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">予算など</div>
                            <div class="wizard-step-text-details">予算や期間</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="progress wizard-bar">
                <div class="progress-bar wizard-33" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-33-66" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-66-85" role="progressbar" style="width: 25%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-none" role="progressbar" style="width: 8%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <form method="post" action="{{ route('project_setup_finish') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="name" value="{{ $name }}" />
                <input type="hidden" name="description" value="{{ $description }}" />
                <input type="hidden" name="category" value="{{ $category }}" />
                @foreach ($skill as $i => $s)
                <input type="hidden" name="skill[]" value="{{ $s }}" />
                @endforeach
                <div class="card-body mt-4">
                    <h3 class="wizard-title">ステップ3</h3>
                    <h5 class="wizard-sub-title">プロジェクトの予算や期間を入力しよう</h5>

                    <div class="form-row">
                        <div class="form-group col-xl-6">
                            <label for="budget_hourly" class="font-weight-bold">最低時給*</label>
                            <div class="input-group mb-3">
                                <input type="text" name="budget_hourly" id="budget_hourly" class="form-control @error('budget_hourly') is-invalid @enderror" value="{{ old('budget_hourly') }}">
                                <div class="input-group-append">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-xl-6">
                            <label for="budget_total" class="font-weight-bold">予算*</label>
                            <div class="input-group mb-3">
                                <input type="text" name="budget_total" id="budget_total" class="form-control @error('budget_total') is-invalid @enderror" value="{{ old('budget_total') }}">
                                <div class="input-group-append">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xl-6">
                            <label for="project_length" class="font-weight-bold">期間*</label>
                            <select class="form-control @error('project_length') is-invalid @enderror" required id="project_length" name="project_length">
                                <option value="" {{ old('project_length') == '' || !old('project_length') ? 'selected' : '' }}>未定</option>
                                <option value="1" {{ old('project_length') == '1' ? 'selected' : '' }}>1ヶ月以内</option>
                                <option value="2" {{ old('project_length') == '2' ? 'selected' : '' }}>1〜3ヶ月</option>
                                <option value="3" {{ old('project_length') == '3' ? 'selected' : '' }}>3〜6ヶ月</option>
                                <option value="4" {{ old('project_length') == '4' ? 'selected' : '' }}>半年以上</option>
                            </select>
                        </div>

                        <div class="form-group col-xl-6">
                            <label for="project_status" class="font-weight-bold">ステータス*</label>
                            <select class="form-control @error('project_status') is-invalid @enderror" required id="project_status" name="project_status">
                                <option value="1" {{ old('project_status') == '1' || !old('project_status') ? 'selected' : '' }}>今人手募集中</option>
                                <option value="2" {{ old('project_status') == '2' ? 'selected' : '' }}>募集終了</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="button" onclick="event.preventDefault(); document.getElementById('prev-form').submit();" class="btn-common cancel mr-3 text-decoration-none">
                        前へ
                    </button>
                    <button type="submit" class="btn-common orange float-right">
                        保存
                    </button>
                </div>
            </form>
        </div>
    </div>

    <form method="post" action="{{ route('project_setup_step2') }}" id="prev-form">
        {{ csrf_field() }}
        <input type="hidden" name="name" value="{{ $name }}" />
        <input type="hidden" name="description" value="{{ $description }}" />
        <input type="hidden" name="category" value="{{ $category }}" />
        @foreach ($skill as $i => $s)
        <input type="hidden" name="skill[]" value="{{ $s }}" />
        @endforeach
    </form>

    <form method="post" action="{{ route('project_setup_start') }}" id="step1-form">
        {{ csrf_field() }}
        <input type="hidden" name="name" value="{{ $name }}" />
        <input type="hidden" name="description" value="{{ $description }}" />
        <input type="hidden" name="category" value="{{ $category }}" />
        @foreach ($skill as $i => $s)
        <input type="hidden" name="skill[]" value="{{ $s }}" />
        @endforeach
    </form>
@endsection
