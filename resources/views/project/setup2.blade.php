<?php $isInCreateProject = true; ?>

@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'logo', 'title' => '新規プロジェクト'])

    <div class="container-fluid pt-5 px-md-10">
        <div class="card">
            <div class="card-header nav-wizard-tab">
                <div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                    <a class="nav-item nav-link" href="#" onclick="event.preventDefault(); document.getElementById('prev-form').submit();">
                        <div class="wizard-step-icon">1</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">基本情報</div>
                            <div class="wizard-step-text-details">プロジェクト名と説明</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link active">
                        <div class="wizard-step-icon">2</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">必要なスキル</div>
                            <div class="wizard-step-text-details">スキルに関する情報</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link nav-wizard-next">
                        <div class="wizard-step-icon">3</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">予算など</div>
                            <div class="wizard-step-text-details">予算や時期</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="progress wizard-bar">
                <div class="progress-bar wizard-33" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-33-66" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-none" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <form method="post" action="{{ route('project_setup_step3') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="name" value="{{ $name }}" />
                <input type="hidden" name="description" value="{{ $description }}" />
                <input type="hidden" name="category" value="{{ $category }}" />
                <div class="card-body mt-4">
                    <h3 class="wizard-title">ステップ2</h3>
                    <h5 class="wizard-sub-title">プロジェクトに必要スキルを追加しよう</h5>

                    <div id="skill-form">
                        <label class="font-weight-bold">スキルを追加</label>
                        <?php $skills = old('skill') ?? $skill; ?>
                        @if ($skills)
                        @foreach ($skills as $i => $s)
                        <div class="form-row" id="skill-row-{{ $i }}">
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <input type="text" id="skill-name-{{ $i }}" name="skill[]" class="form-control @error('skill.'.$i) is-invalid @enderror" value="{{ $s }}">
                                    <div class="input-group-append">
                                        <button type="button" class="btn-common btn-close btn-skill-remove" onclick="removeSkill(event, {{ $i }})">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                            <div class="form-row" id="skill-row-0">
                                <div class="col-12">
                                    <div class="input-group mb-3">
                                        <input type="text" id="skill-name-0" name="skill[]" class="form-control @error('skill0') is-invalid @enderror" value="">
                                        <div class="input-group-append">
                                            <button type="button" class="btn-common btn-close btn-skill-remove" onclick="removeSkill(event, 0)">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="form-group text-right">
                        <button type="button" class="btn-common outline-orange" onclick="addSkill(event)">スキル追加</button>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="button" onclick="event.preventDefault(); document.getElementById('prev-form').submit();" class="btn-common cancel mr-3 text-decoration-none">
                        前へ
                    </button>
                    <button type="submit" class="btn-common orange float-right">
                        次へ
                    </button>
                </div>
	            <div class="card-header nav-wizard-tab nav-wizard-xl">
                	<div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                        <a class="nav-item nav-link">
                            <div class="wizard-step-icon">3</div>
                            <div class="wizard-step-text">
                                <div class="wizard-step-text-name">予算など</div>
                                <div class="wizard-step-text-details">予算や時期</div>
                            </div>
                        </a>
                    </div>
	            </div>
            </form>
        </div>
    </div>

<script id="skill-form-template" type="text/mustache-tmpl">
    <div class="form-row" id="skill-row-<% skill_idx %>">
        <div class="col-12">
            <div class="input-group mb-3">
                <input type="text" id="skill-name-<% skill_idx %>" name="skill[]" class="form-control">
                <div class="input-group-append">
                    <button type="button" class="аe btn-common btn-close btn-skill-remove" onclick="removeSkill(event, <% skill_idx %>)">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                </div>
            </div>
        </div>
    </div>
</script>

<script>
    var skill_idx = {{ old('skill') && sizeof(old('skill')) ? sizeof(old('skill')) : '0' }};
    function addSkill(e) {
        e.preventDefault();
        var div = document.createElement('div');
        div.innerHTML = Mustache.render(document.getElementById('skill-form-template').innerHTML, {skill_idx: skill_idx}).trim();
        document.getElementById('skill-form').appendChild(div.firstChild);
        skill_idx++;
    }

    function removeSkill(e, idx) {
        e.preventDefault();
        document.getElementById('skill-row-' + idx).remove();
    }
</script>

    <form method="post" action="{{ route('project_setup_start') }}" id="prev-form">
        {{ csrf_field() }}
        <input type="hidden" name="name" value="{{ $name }}" />
        <input type="hidden" name="description" value="{{ $description }}" />
        <input type="hidden" name="category" value="{{ $category }}" />
        @if ($skills)
        @foreach ($skill as $i => $s)
        <input type="hidden" name="skill[]" value="{{ $s }}" />
        @endforeach
        @endif
    </form>
@endsection
