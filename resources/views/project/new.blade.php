<?php $isInCreateProject = true; ?>

@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'logo', 'title' => '新規プロジェクト'])

    <div class="container pt-5">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="card sb-card-header-actions mx-auto">
            <div class="card-header">新規プロジェクト</div>
            <form method="post" action="{{ route('new_project') }}" enctype="multipart/form-data">
            <div class="card-body pb-5">
                    {{ csrf_field() }}

                    <div class="card card-block">
                        <div class="card-header">基本情報</div>
                        <div class="card-body">

                            <div class="form-group">
                                <label for="name" class="font-weight-bold">プロジェクト名*</label>
                                <input name="name" class="form-control @error('name') is-invalid @enderror" id="name" required type="text" value="{{ old('name') }}">
                            </div>

                            <div class="form-group">
                                <label for="category" class="font-weight-bold">カテゴリー</label>
                                <select class="form-control @error('category') is-invalid @enderror" id="category" name="category">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" {{ old('category') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="description" class="font-weight-bold">説明</label>
                                <textarea rows="5" id="description" name="description" required class="form-control @error('description') is-invalid @enderror">{{ old('description') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="group" class="font-weight-bold">グループ</label>
                                <select class="form-control @error('group') is-invalid @enderror" id="group" name="group">
                                    <option value="" {{ old('group') == '' || !old('group') ? 'selected' : '' }}>なし</option>
                                    @foreach (Auth::user()->groups as $group)
                                    <option value="{{ $group->id }}" {{ old('group') == $group->id ? 'selected' : '' }}>{{ $group->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="privacy" class="font-weight-bold">公開設定*</label>
                                <select class="form-control @error('privacy') is-invalid @enderror" required id="privacy" name="privacy">
                                    <option value="1" {{ (old('privacy')) == '1' ? 'selected' : '' }}>全体公開</option>
                                    <option value="2" {{ (old('privacy')) == '2' ? 'selected' : '' }}>グループ公開</option>
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="card card-block my-5">
                        <div class="card-header">必要スキル</div>
                        <div class="card-body">
                            <div id="skill-form">
                                <label class="font-weight-bold">スキルを追加</label>
                                @if (old('skill'))
                                @foreach (old('skill') as $i => $skill)
                                <div class="form-row" id="skill-row-{{ $i }}">
                                    <div class="col-12">
                                        <div class="input-group mb-3">
                                            <input type="text" id="skill-name-{{ $i }}" name="skill[]" class="form-control @error('skill.'.$i) is-invalid @enderror" value="{{ $skill }}">
                                            <div class="input-group-append">
                                                <button type="button" class="btn-common btn-close btn-skill-remove" onclick="removeSkill(event, {{ $i }})">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @else
                                    <div class="form-row" id="skill-row-0">
                                        <div class="col-12">
                                            <div class="input-group mb-3">
                                                <input type="text" id="skill-name-0" name="skill[]" class="form-control @error('skill0') is-invalid @enderror" value="">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn-common btn-close btn-skill-remove" onclick="removeSkill(event, 0)">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="form-group text-right">
                                <button type="button" class="btn-common outline-orange" onclick="addSkill(event)">スキル追加</button>
                            </div>
                        </div>
                    </div>

                    <div class="card card-block">
                        <div class="card-header">予算など</div>
                        <div class="card-body">

                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="budget_hourly" class="font-weight-bold">最低時給*</label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="budget_hourly" id="budget_hourly" class="form-control @error('budget_hourly') is-invalid @enderror" value="{{ old('budget_hourly') }}">
                                        <div class="input-group-append">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label for="budget_total" class="font-weight-bold">予算*</label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="budget_total" id="budget_total" class="form-control @error('budget_total') is-invalid @enderror" value="{{ old('budget_total') }}">
                                        <div class="input-group-append">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="project_length" class="font-weight-bold">期間*</label>
                                    <select class="form-control @error('project_length') is-invalid @enderror" required id="project_length" name="project_length">
                                        <option value="" {{ old('project_length') == '' || !old('project_length') ? 'selected' : '' }}>未定</option>
                                        <option value="1" {{ old('project_length') == '1' ? 'selected' : '' }}>1ヶ月以内</option>
                                        <option value="2" {{ old('project_length') == '2' ? 'selected' : '' }}>1〜3ヶ月</option>
                                        <option value="3" {{ old('project_length') == '3' ? 'selected' : '' }}>3〜6ヶ月</option>
                                        <option value="4" {{ old('project_length') == '4' ? 'selected' : '' }}>半年以上</option>
                                    </select>
                                </div>

                                <div class="form-group col-6">
                                    <label for="project_status" class="font-weight-bold">ステータス*</label>
                                    <select class="form-control @error('project_status') is-invalid @enderror" required id="project_status" name="project_status">
                                        <option value="2" {{ old('project_status') == '2' || !old('project_status') ? 'selected' : '' }}>募集終了</option>
                                        <option value="1" {{ old('project_status') == '1' ? 'selected' : '' }}>今人手募集中</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="/home" class="btn-common cancel mr-3 text-decoration-none">
                        キャンセル
                    </a>
                    <button type="submit" class="btn-common orange float-right">
                        保存
                    </button>
                </div>
            </form>
        </div>
    </div>

<script id="skill-form-template" type="text/mustache-tmpl">
    <div class="form-row" id="skill-row-<% skill_idx %>">
        <div class="col-12">
            <div class="input-group mb-3">
                <input type="text" id="skill-name-<% skill_idx %>" name="skill[]" class="form-control">
                <div class="input-group-append">
                    <button type="button" class="аe btn-common btn-close btn-skill-remove" onclick="removeSkill(event, <% skill_idx %>)">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                </div>
            </div>
        </div>
    </div>
</script>

<script>
    var skill_idx = {{ old('skill') && sizeof(old('skill')) ? sizeof(old('skill')) : '0' }};
    function addSkill(e) {
        e.preventDefault();
        var div = document.createElement('div');
        div.innerHTML = Mustache.render(document.getElementById('skill-form-template').innerHTML, {skill_idx: skill_idx}).trim();
        document.getElementById('skill-form').appendChild(div.firstChild);
        skill_idx++;
    }

    function removeSkill(e, idx) {
        e.preventDefault();
        document.getElementById('skill-row-' + idx).remove();
    }
</script>
@endsection
