@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow-lg border-0 rounded-lg mt-5 p-3">
              <form method="POST" action="{{ route('create_invitation_url.create',[]) }}">
                  @csrf
                  <p>＊有効期限は24時間</p>
                  <div class="form-group mt-4 mb-4 d-flex justify-content-center">
                      <button type="submit" class="btn-common orange">
                          招待URLを生成する
                      </button>
                  </div>
              </form>

              <div>{{ $create_url }}</div>
            </div>
        </div>
    </div>
</div>
@endsection
