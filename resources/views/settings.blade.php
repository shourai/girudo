@extends('layouts.app')

@section('content')
@include('components.content_top_block', ['icon' => 'user', 'title' => '設定'])
<div class="container pt-5">
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card">
        <div class="card-header">設定</div>
        <form method="post" action="{{ route('settings') }}">
            <div class="card-body">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="email" class="font-weight-bold">メール</label>
                    <input name="email" class="form-control @error('email') is-invalid @enderror" id="email" type="email" value="{{ old('email') ?? $user->email }}" required autocomplete="email">
                </div>

                <div class="form-group">
                    <label for="password" class="font-weight-bold">現在のパスワード</label>
                    <input name="password" class="form-control @error('password') is-invalid @enderror" id="password" type="password">
                </div>

                <div class="form-group">
                    <label for="new-password" class="font-weight-bold">新しいパスワード</label>
                    <input name="new-password" class="form-control @error('new-password') is-invalid @enderror" id="new-password" type="password">
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="font-weight-bold">新しいパスワードを再入力</label>
                    <input name="new-password_confirmation" class="form-control @error('new-password_confirmation') is-invalid @enderror" id="password-confirm" type="password">
                </div>

                <div class="form-group">
                    <label class="font-weight-bold">メール受信設定</label>

                    <div class="form-check">
                        <input name="notification_project_message" class="form-check-input" type="checkbox" value="1" {{ old('notification_project_message') !== null ? old('notification_project_message') ? 'checked' : '' : $user->notification_project_message ? 'checked' : '' }} id="notification_project_message">
                        <label class="form-check-label font-weight-bold" for="notification_project_message">
                            募集中のプロジェクトへのメッセージ
                        </label>
                    </div>

                    <div class="form-check">
                        <input name="notification_user_message" class="form-check-input" type="checkbox" value="1" {{ old('notification_user_message') !== null ? old('notification_user_message') ? 'checked' : '' : $user->notification_user_message ? 'checked' : '' }} id="notification_user_message">
                        <label class="form-check-label font-weight-bold" for="notification_user_message">
                            ダイレクトメッセージ
                        </label>
                    </div>
                </div>

                <div class="form-group mt-4">
                    <a href="/users/{{ Auth::id() }}/delete_confirm" class="btn btn-sm text-danger font-weight-bold">
                        アカウントを削除
                    </a>
                </div>

            </div>
            <div class="card-footer d-flex justify-content-end">
                <a href="/home" class="btn-common cancel mr-3 text-decoration-none">
                    キャンセル
                </a>
                <button type="submit" class="btn-common orange float-right">
                    保存
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
