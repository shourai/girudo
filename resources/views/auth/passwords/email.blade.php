@extends('layouts.authentication')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card shadow-lg border-0 rounded-lg mt-5 guil-login-block">
                <div class="card-header justify-content-center">
                    <h2 class="my-4">パスワードを再設定する</h2>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                        <span  class="user-icon guil-icon"></span>
                            <input id="email" placeholder="メール" type="email" class="form-control py-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group d-flex align-items-center justify-content-between forgot mt-4 mb-0">
                            <a class="small" href="{{ route('login') }}">
                            ログインに戻る
                            </a>
                            <button type="submit" class="btn button1">
                            パスワードを再設定する
                            </button>
                        </div>
                    </form>
                </div>

                @if (Route::has('register'))
                <div class="card-footer text-center">
                    <div class="small orange">
                        <a href="{{ route('unable_register') }}">アカウントを作成するにはここをクリックしてください</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
