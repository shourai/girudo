@extends('layouts.authentication')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="card shadow-lg border-0 rounded-lg mt-5 guil-login-block">
                <div class="card-header justify-content-center">
                    <h2>アカウント作成</h2>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group">
                            <!-- <label for="name" class="small mb-1">名前</label> -->
                            <span  class="user-icon guil-icon"></span>
                            <input id="name" placeholder="名前" type="text" class="form-control py-4 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <!-- <label for="email" class="small mb-1">メール</label> -->
                            <span  class="user-icon guil-icon"></span>
                            <input id="email" placeholder="メール" type="email" class="form-control py-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label for="password" class="small mb-1">パスワード</label> -->
                                    <span  class="lock-icon guil-icon"></span>
                                    <input id="password" placeholder="パスワード" type="password" class="form-control py-4 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label for="password-confirm" class="small mb-1">パスワードの確認</label> -->
                                    <span  class="lock-icon guil-icon"></span>
                                    <input id="password-confirm" placeholder="パスワードの確認" type="password" class="form-control py-4" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                        </div>

                        <div class="form-group mt-4 mb-0 text-center">
                            <button type="submit" class="btn button1">
                                アカウント作成
                            </button>
                        </div>
                    </form>
                </div>

                <div class="card-footer text-center">
                    <div class="orange">
                        <a href="{{ route('login') }}">ログインに戻る</a>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
