@extends('layouts.authentication')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 mb-5">
            <div class="card my-10">
                <div class="card-header">メールを確認してください</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            仮会員メールのメールをお送りいたしました。
                        </div>
                    @endif

                    <div class="form-group">
                        <label>
                            現在、仮会員の状態です。ただいま、ご入力いただいたメールアドレス宛に、ご本人様確認用のメールをお送りいたしました。<br>メール本文内のURLをクリックすると、本会員登録が完了となります。
                        </label>
                    </div>

                    <div class="form-group">
                        <label>
                            万一、仮会員メールが届かない場合は下記のの「仮会員メール再送」ボタンをクリックしてください。
                        </label>
                    </div>

                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <div class="form-group text-center mt-4 mb-0">
                            <button type="submit" class="btn button1 p-0 m-0 align-baseline">仮会員メール再送</button>
                        </div>
                    </form>
                </div>

                <div class=" card-footer pt-0  text-center">
                    <div class="small orange">
                        <a href="{{ url('/') }}">ホームページに戻る</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
