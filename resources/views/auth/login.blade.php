@extends('layouts.authentication')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class=" mt-5">
            <div class="logo">
                <img src="{{ asset('images/Logo.png') }}" class="img-fluid">
            </div>
            <div class="card shadow-lg border-0 rounded-lg guil-login-block">
                <div class="card-header justify-content-center">
                    <h2>ログイン</h2>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <span  class="user-icon guil-icon"></span>
                            <input id="email" type="email" placeholder="メール" class="form-control py-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                        <span  class="lock-icon guil-icon"></span>

                            <input id="password" type="password" placeholder="パスワード" class="form-control py-4 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="custom-control-label" for="remember">
                                    次回から自動的にログインする
                                </label>
                            </div>
                        </div>

                        <div class="form-group text-center mt-4 mb-0">
                            <button type="submit" class="btn button1">
                                ログイン
                            </button>
                            @if (Route::has('password.request'))
                                <a class="small" href="{{ route('password.request') }}">
                                    ログイン情報をお忘れですか？
                                </a>
                            @endif
                        </div>
                    </form>
                </div>

                @if (Route::has('register'))
                <div class="card-footer text-center pb-0">
                    <h4>ギルドは初めてですか？</h4>
                    <div class="small">
                        <a class="sign-up signup btn" href="{{ route('unable_register') }}">会員登録（無料）</a>
                    </div>
                </div>
                @endif
                <div class=" card-footer pt-0  text-center">
                    <div class="small orange">
                        <a href="{{ url('/') }}">ホームページに戻る</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
