@extends('layouts.app')

@section('content')
@include('components.content_top_block', ['icon' => 'user', 'title' => 'プロフィール', 'test' => '編集' ])
<div class="container-fluid pt-5">
    <div class="card sb-card-header-actions mx-auto">
        <div class="card-header">
            プロフィール編集
            {{--
            <div>
                <a href="/users/{{ Auth::id() }}/delete_confirm" class="btn-common btn-close text-decoration-none">
                    @include('components.icons.useSprite', ['name' => 'cross'])
                </a>
            </div>
            --}}
        </div>
        <form method="post" action="{{ route('edit_profile', ['user_id' => Auth::id()]) }}" enctype="multipart/form-data">
            <div class="card-body">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name" class="font-weight-bold">名前</label>
                    <input name="name" class="form-control @error('name') is-invalid @enderror" id="name" type="text" value="{{ old('name') ?? $user->name }}" required autocomplete="name" autofocus>
                </div>

                <div class="form-group" id="SNS">
                    <label for="bio" class="font-weight-bold">自己紹介</label>
                    <textarea name="bio" class="form-control @error('bio') is-invalid @enderror" id="bio" rows="3">{{ old('bio') ?? $user->bio }}</textarea>
                </div>

                <div class="form-group">
                    <label for="twitter" class="font-weight-bold">Twitter</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="text" name="twitter" class="form-control @error('twitter') is-invalid @enderror" id="twitter" value="{{ old('twitter') ?? $user->twitter }}" placeholder="ユーザ名">
                    </div>
                </div>


                <div class="form-group">
                    <label for="github" class="font-weight-bold">Github</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="text" name="github" class="form-control @error('github') is-invalid @enderror" id="github" value="{{ old('github') ?? $user->github }}" placeholder="ユーザ名">
                    </div>
                </div>

                <div class="form-group">
                    <label for="linkedin" class="font-weight-bold">LinkedIn</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">https://www.linkedin.com/in/</span>
                        </div>
                        <input type="text" name="linkedin" class="form-control @error('linkedin') is-invalid @enderror" id="linkedin" value="{{ old('linkedin') ?? $user->linkedin }}" placeholder="ユーザ名">
                    </div>
                </div>

                <div class="form-group">
                    <label for="website" class="font-weight-bold">サイト</label>
                    <input type="text" name="website" class="form-control @error('website') is-invalid @enderror" id="website" value="{{ old('website') ?? $user->website }}" placeholder="https://guildoh.com/">
                </div>

                <div class="form-group">
                    <label for="profile-pic" class="font-weight-bold">写真</label>
                    <!--input type="file" name="profile_pic" id="profile-pic" class="form-control @error('profile_pic') is-invalid @enderror"-->

                    <div class="input-group mb-3 @error('profile_pic') is-invalid @enderror">
                        <div class="custom-file">
                            <input accept="image/*" type="file" name="profile_pic" id="profile-pic" class="custom-file-input form-control @error('profile_pic') is-invalid @enderror">
                            <label class="custom-file-label font-weight-bold" >ファイルを選択</label>
                        </div>
                    </div>

                    @if ($errors->has('profile_pic'))
                    <div class="invalid-feedback">
                        {{ $errors->first('profile_pic') }}
                    </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="availability" class="font-weight-bold">ステータス</label>
                    <select class="form-control @error('availability') is-invalid @enderror" id="availability" name="availability">
                        <option value="1" {{ (old('availability') ?? $user->availability) == '1' ? 'selected' : '' }}>リソース余裕あり</option>
                        <option value="2" {{ (old('availability') ?? $user->availability) == '2' ? 'selected' : '' }}>リソース余裕なし</option>
                    </select>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end">
                <a href="/users/{{ Auth::id() }}" class="btn-common cancel mr-3 text-decoration-none">
                    キャンセル
                </a>
                <button type="submit" class="btn-common orange float-right">
                    保存
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
