@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'user', 'title' => $user->name, 'status' => $user->availability ])
    <div class="container-fluid pt-5">
        @if (session('success'))
            <div class="alert alert-success mx-4">
                {{ session('success') }}
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center">
                    <h5 class="info-project-title">自己紹介</h5>
                    @auth()
                        @if ($user->id == Auth::id())
                            <div class="mr-0 mr-sm-4">
                                <a class="btn-common orange text-decoration-none text-nowrap"
                                   href="{{ route('edit_profile', ['user_id' => $user->id]) }}">編集</a>
                            </div>
                        @endif
                    @endauth
                </div>
                <div class="info-block-user main-project-info mx-0 mx-sm-4">
                    @if ($user->bio)
                        {{ $user->getBio() }}
                    @else
                        <p>なし</p>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center">
                    <h5 class="info-project-title">スキル一覧 <small>(チームメンバーがいる人はここでチームメンバーのスキルも登録してください)</small></h5>
                    @auth()
                        @if ($user->id == Auth::id())
                            <div class="mr-0 mr-sm-4">
                                <button class="btn-common orange text-nowrap" type="button" data-toggle="modal"
                                        data-target="#skill-modal">追加
                                </button>
                            </div>
                        @endif
                    @endauth
                </div>
                <div class="info-block-user main-project-info mx-0 mx-sm-4">
                    <ul class="pl-0">
                        @forelse ($user->skills as $skill)
                        <li>
                            {{ $skill->name }} ({{ $skill->pivot->levelStr() }})
                            @if ($user->id == Auth::id())
                            &nbsp;<a onClick="event.preventDefault(); if (!confirm('スキル「{{ $skill->name }}」を削除しますか？')) return; document.getElementById('delete-skill-id').value = '{{ $skill->name }}'; document.getElementById('delete-skill-level').value = '{{ $skill->pivot->level }}'; document.getElementById('delete-skill-form').submit();" href="#"><i data-feather="delete" class="align-middle"></i></a>
                            @endif
                        </li>
                        @empty
                            <p>なし</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center">
                    <h5 class="info-project-title">Guildoh以外の関わったプロジェクト一覧</h5>
                    @auth()
                        @if ($user->id == Auth::id())
                            <div class="mr-0 mr-sm-4">
                                <button class="btn-common orange text-nowrap" type="button" data-toggle="modal"
                                        data-target="#project-modal">追加
                                </button>
                            </div>
                        @endif
                    @endauth
                </div>
                <div class="info-block-user main-project-info mx-0 mx-sm-4">
                    <ul class="pl-0">
                        @forelse ($user->extraProjects as $project)
                            <li>{{ $project->outline }}</li>
                        @empty
                            <p>なし</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>

        @if ($user->twitter || $user->github || $user->linkedin)
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex flex-sm-row justify-content-between align-items-center">
                        <h5 class="info-project-title">SNS</h5>
                        @auth()
                            @if ($user->id == Auth::id())
                                <div class="mr-0 mr-sm-4">
                                    @if ($user->id == Auth::id())
                                        <a class="btn-common orange text-decoration-none text-nowrap"
                                           href="{{ route('edit_profile', ['user_id' => $user->id]) }}#SNS">編集</a>
                                    @endif
                                </div>
                            @endif
                        @endauth
                    </div>
                    <div class="info-block-user main-project-info mx-0 mx-sm-4">
                        <ul class="pl-0">
                            @if ($user->twitter)
                                <li>Twitter: <a href="https://twitter.com/{{ $user->twitter }}"
                                                target="_blank">{{ '@' }}{{ $user->twitter }}</a></li>
                            @endif
                            @if ($user->github)
                                <li>Github: <a href="https://github.com/{{ $user->github }}"
                                               target="_blank">{{ '@' }}{{ $user->github }}</a></li>
                            @endif
                            @if ($user->linkedin)
                                <li>LinkedIn: <a href="https://www.linkedin.com/in/{{ $user->linkedin }}"
                                                 target="_blank">{{ 'https://www.linkedin.com/in/' }}{{ $user->linkedin }}</a>
                                </li>
                            @endif
                            @if ($user->website)
                                <li>サイト: <a href="{{ $user->website }}" target="_blank">{{ $user->website }}</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center">
                    <h5 class="info-project-title">現在のGuildohのプロジェクト一覧</h5>
                    @auth()
                        @if ($user->id == Auth::id())
                            <div class="mr-0 mr-sm-4">
                                <a class="btn-common orange text-decoration-none text-nowrap" href="{{ route('new_project') }}">追加</a>
                            </div>
                        @endif
                    @endauth
                </div>
                <div class="info-block-user main-project-info mx-0 mx-sm-4">
                    <ul class="pl-0">
                        @forelse ($user->currentProjects as $project)
                            <li class="project-link">
                                <a href="{{ route('project', ['project_id' => $project->id]) }}">{{ $project->name }}</a>
                                @if ($user->id == Auth::id())
                                    <span>
                                        <a class="edit-btn"
                                           href="{{ route('edit_project', ['project_id' => $project->id]) }}"><i
                                                data-feather="edit" class="align-middle"></i></a>
                                    </span>
                                @endif
                            </li>
                        @empty
                            <p>なし</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center">
                    <h5 class="info-project-title">過去のGuildohのプロジェクト一覧</h5>
                </div>
                <div class="info-block-user main-project-info mx-0 mx-sm-4">
                    <ul class="pl-0">
                        @forelse ($user->pastProjects as $project)
                            <li class="project-link">
                                <a href="{{ route('project', ['project_id' => $project->id]) }}">{{ $project->name }}</a>
                            </li>
                        @empty
                            <p>なし</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>

        @if ($user->ownTeam)
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex flex-sm-row justify-content-between align-items-center">
                        <h5 class="info-project-title">チームメンバー</h5>
                        @auth()
                            @if ($user->id == Auth::id())
                                <div class="mr-0 mr-sm-4">
                                    <a class="btn-common orange text-decoration-none text-nowrap"
                                       href="{{ route('profile_add_member', ['user_id' => $user->id]) }}">追加</a>
                                </div>
                            @endif
                        @endauth
                    </div>
                    <div class="info-block-user main-project-info mx-0 mx-sm-4">
                        <ul class="pl-0">
                            @forelse ($user->ownTeam->teamMembers as $member)
                                <li>{{ $member->name }}</li>
                            @empty
                                <p>なし</p>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        @auth
            @if (Auth::id() != $user->id)
                <div class="row">
                    <div class="col-md-12">
                        <div class="d-flex flex-sm-row justify-content-between align-items-center flex-column">
                            <h5 class="info-project-title">メッセージ</h5>
                        </div>
                        <div class="info-block-user main-project-info mx-0 mx-sm-4">
                            <a class="btn-common mail-text text-decoration-none"
                               href="{{ route('user_message', ['user_id' => $user->id, 'initiator_id' => Auth::id()]) }}">@include('components.icons.useSprite', ['name' => 'mail'])
                                メッセージ送る</a>
                        </div>
                    </div>
                </div>
            @endif
        @endauth

        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center flex-column">
                    <h5 class="info-project-title">所属グループ</h5>
                    @auth()
                        @if ($user->id == Auth::id())
                            <div class="mr-0 mr-sm-4">
                                <button class="btn-common orange text-nowrap" type="button" data-toggle="modal"
                                        data-target="#group-modal">追加
                                </button>
                            </div>
                        @endif
                    @endauth
                </div>
                <div class="info-block-user main-project-info mx-0 mx-sm-4">
                    <ul class="pl-0">
                        @forelse ($user->groups as $group)
                        <li>
                            <a href="{{ route('group', ['group_id' => $group->id]) }}">{{ $group->name }}</a>
                            @if ($user->id == Auth::id() && !$group->isAdmin(Auth::id()))
                            &nbsp;<a onClick="event.preventDefault(); if (!confirm('グループ「{{ $group->name }}」を退出しますか？')) return; document.getElementById('delete-group-id').value = '{{ $group->id }}'; document.getElementById('delete-group-form').submit();" href="#"><i data-feather="delete" class="align-middle"></i></a>
                            @endif
                            @if ($group->isAdmin(Auth::id()))
                            &nbsp;<span>
                                <a class="edit-btn"
                                   href="{{ route('group_edit', ['group_id' => $group->id]) }}"><i
                                        data-feather="edit" class="align-middle"></i></a>
                            </span>
                            @endif
                        </li>
                        @empty
                            <p>なし</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>

    </div>

    @if ($user->id == Auth::id())
        <div class="modal fade" id="skill-modal">
            <div class="modal-dialog">
                <div class="modal-common">
                    <div class="modal-header modal-header-common d-flex flex-column">
                        <button class="close font-weight-100" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h5 class="modal-common-title">スキル追加</h5>
                    </div>
                    <form method="POST" action="{{ route('profile_add_skill', ['user_id' => $user->id]) }}">
                        {{ csrf_field() }}
                        <div class="modal-body modal-body-common">
                            <div class="form-group">
                                <label for="skill-name">スキル</label>
                                <input type="text" id="skill-name" name="skill" class="form-control @error('skill', 'skill') is-invalid @enderror"  required>
                                @error('skill', 'skill')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="skill-level">スキルレベル</label>
                                <select class="form-control @error('skill_level', 'skill') is-invalid @enderror" id="skill-level" name="skill_level" >
                                    <option value="1">初心者</option>
                                    <option value="2">中級者</option>
                                    <option value="3">エキスパート</option>
                                </select>
                                @error('skill_level', 'skill')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer-common">
                            <button class="btn-common cancel mr-2" type="button" data-dismiss="modal">キャンセル</button>
                            <button class="btn-common orange">保存</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

    @if ($user->id == Auth::id())
        <div class="modal fade" id="project-modal">
            <div class="modal-dialog">
                <div class="modal-common">
                    <div class="modal-header modal-header-common d-flex flex-column">
                        <button class="close font-weight-100" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h5 class="modal-common-title">プロジェクト追加</h5>
                    </div>
                    <form method="POST" action="{{ route('profile_add_extra_project', ['user_id' => $user->id]) }}">
                        {{ csrf_field() }}
                        <div class="modal-body modal-body-common">
                            <div class="form-group">
                                <label for="extra-project-outline">プロジェクト概要</label>
                                <textarea required id="extra-project-outline" name="outline" class="form-control @error('outline', 'project') is-invalid @enderror"
                                          row=3></textarea>
                                @error('outline', 'project')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer-common">
                            <button class="btn-common cancel mr-2" type="button" data-dismiss="modal">キャンセル</button>
                            <button class="btn-common orange">保存</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

    @if ($user->id == Auth::id())
        <div class="modal fade" id="group-modal">
            <div class="modal-dialog">
                <div class="modal-common">
                    <div class="modal-header modal-header-common d-flex flex-column">
                        <button class="close font-weight-100" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h5 class="modal-common-title">グループ設立</h5>
                    </div>
                    <form method="POST" action="{{ route('profile_new_group', ['user_id' => $user->id]) }}">
                        {{ csrf_field() }}
                        <div class="modal-body modal-body-common">
                            <div class="form-group">
                                <label for="group-name">グループ名</label>
                                <input type="text" required id="group-name" name="name" class="form-control @error('name', 'group') is-invalid @enderror" />
                                @error('name', 'group')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="group-description">グループ紹介文</label>
                                <textarea required id="group-description" name="description" class="form-control @error('description', 'group') is-invalid @enderror"
                                          row=3></textarea>
                                @error('description', 'group')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="group-privacy">公開設定</label>
                                <select class="form-control @error('privacy', 'group') is-invalid @enderror" id="group-privacy" name="privacy" >
                                    <option value="1">公開</option>
                                    <option value="2">招待制</option>
                                </select>
                                @error('privacy', 'group')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer-common">
                            <button class="btn-common cancel mr-2" type="button" data-dismiss="modal">キャンセル</button>
                            <button class="btn-common orange">保存</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

    @if ($user->id == Auth::id())
    <form id="delete-skill-form" action="{{ route('profile_delete_skill', ['user_id' => Auth::id()]) }}" method="POST">
        @csrf
        <input type="hidden" name="skill_id" value="" id="delete-skill-id" />
        <input type="hidden" name="skill_level" value="" id="delete-skill-level" />
    </form>

    <form id="delete-group-form" action="{{ route('group_exit', ['user_id' => Auth::id()]) }}" method="POST">
        @csrf
        <input type="hidden" name="group_id" value="" id="delete-group-id" />
    </form>
    @endif

    @if ($errors->skill->any())
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('#skill-modal').modal('show');
    });
</script>
    @endif

    @if ($errors->project->any())
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('#project-modal').modal('show');
    });
</script>
    @endif
@endsection
