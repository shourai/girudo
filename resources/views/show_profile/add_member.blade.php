@extends('layouts.app')

@section('content')
@include('components.content_top_block', ['icon' => 'user', 'title' => 'チームメンバー', 'test' => '追加' ])
<div class="container-fluid pt-5">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="card">
        <div class="card-header">チームメンバー追加</div>
        <div class="card-body">
            <form method="post" action="{{ route('profile_add_member', ['user_id' => $user->id]) }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name" class="font-weight-bold">名前</label>
                    <input name="name" class="form-control @error('name') is-invalid @enderror" id="name" type="text" value="{{ old('name') }}" required>
                </div>

                <div class="form-group">
                    <label for="email" class="font-weight-bold">メール</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password" class="small mb-1 font-weight-bold">パスワード</label>

                            <input id="password" type="password" class="form-control py-4 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password-confirm" class="small mb-1 font-weight-bold">パスワード確認</label>

                            <input id="password-confirm" type="password" class="form-control py-4" name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="button" class="btn-common outline-orange" onclick="addSkill(event)">スキル追加</button>
                </div>

                <div id="skill-form">
                    @if (old('skill'))
                    @foreach (old('skill') as $i => $skill)
                    <div class="form-row" id="skill-row-{{ $i }}">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="skill-name-{{ $i }}" class="font-weight-bold">スキル</label>
                                <input type="text" id="skill-name-{{ $i }}" name="skill[]" class="form-control @error('skill.'.$i) is-invalid @enderror" value="{{ $skill }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="skill-level-{{ $i }}" class="font-weight-bold">スキルレベル</label>
                                <select class="form-control @error('skill.'.$i) is-invalid @enderror" id="skill-level-<% skill_idx %>" name="skill_level[]">
                                    <option value="1" {{ old('skill_level')[$i] == '1' ? 'selected' : '' }}>初心者</option>
                                    <option value="2" {{ old('skill_level')[$i] == '2' ? 'selected' : '' }}>中級者</option>
                                    <option value="3" {{ old('skill_level')[$i] == '3' ? 'selected' : '' }}>エキスパート</option>
                                </select>
                            </div>
                            <div class="text-right">
                                <button type="button" class="btn-common btn-close" onclick="removeSkill(event, {{ $i }})">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>

                <div class="form-group mt-4 d-flex justify-content-center">
                    <button type="submit" class="btn-common orange">
                        追加
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script id="skill-form-template">
    <div class="form-row" id="skill-row-<% skill_idx %>">
        <div class="col-md-8">
            <div class="form-group">
                <label for="skill-name-<% skill_idx %>" class="font-weight-bold">スキル</label>
                <input type="text" id="skill-name-<% skill_idx %>" name="skill[]" class="form-control">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="skill-level-<% skill_idx %>" class="font-weight-bold">スキルレベル</label>
                <select class="form-control" id="skill-level-<% skill_idx %>" name="skill_level[]">
                    <option value="1">初心者</option>
                    <option value="2">中級者</option>
                    <option value="3">エキスパート</option>
                </select>
            </div>
            <div class="text-right">
                <button type="button" class="btn-common btn-close" onclick="removeSkill(event, <% skill_idx %>)">@include('components.icons.useSprite', ['name' => 'cross'])</button>
            </div>
        </div>
    </div>
</script>

<script>
    var skill_idx = {{ old('skill') && sizeof(old('skill')) ? sizeof(old('skill')) : '0' }};
    function addSkill(e) {
        e.preventDefault();
        var div = document.createElement('div');
        div.innerHTML = Mustache.render(document.getElementById('skill-form-template').innerHTML, {skill_idx: skill_idx}).trim();
        document.getElementById('skill-form').appendChild(div.firstChild);
        skill_idx++;
    }

    function removeSkill(e, idx) {
        e.preventDefault();
        document.getElementById('skill-row-' + idx).remove();
    }
</script>
@endsection
