<?php $isInSetup = true; ?>

@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'user', 'title' => 'プロフィール登録'])

    <?php $user = Auth::user() ?>

    <div class="container-fluid pt-5 px-md-10">
        <div class="card">
            <div class="card-header nav-wizard-tab">
                <div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                    <a class="nav-item nav-link" href="{{ route('profile_setup_start') }}">
                        <div class="wizard-step-icon">1</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">プロフィール</div>
                            <div class="wizard-step-text-details">基本情報</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link" href="{{ route('profile_setup_step2') }}">
                        <div class="wizard-step-icon">2</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">スキル</div>
                            <div class="wizard-step-text-details">スキル情報</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link active">
                        <div class="wizard-step-icon">3</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">SNS</div>
                            <div class="wizard-step-text-details">Twitter, Github, LinkedIn</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="progress wizard-bar">
                <div class="progress-bar wizard-33" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-33-66" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-66-85" role="progressbar" style="width: 25%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-none" role="progressbar" style="width: 8%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <form method="post" action="{{ route('profile_setup_finish') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body mt-4">
                    <h3 class="wizard-title">ステップ3</h3>
                    <h5 class="wizard-sub-title">SNSを入力しよう！</h5>

                    <div class="form-group">
                        <label for="twitter" class="font-weight-bold">Twitter</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">@</span>
                            </div>
                            <input type="text" name="twitter" class="form-control @error('twitter') is-invalid @enderror" id="twitter" value="{{ old('twitter') ?? $user->twitter }}" placeholder="ユーザ名">
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="github" class="font-weight-bold">Github</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">@</span>
                            </div>
                            <input type="text" name="github" class="form-control @error('github') is-invalid @enderror" id="github" value="{{ old('github') ?? $user->github }}" placeholder="ユーザ名">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="linkedin" class="font-weight-bold">LinkedIn</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">https://www.linkedin.com/in/</span>
                            </div>
                            <input type="text" name="linkedin" class="form-control @error('linkedin') is-invalid @enderror" id="linkedin" value="{{ old('linkedin') ?? $user->linkedin }}" placeholder="ユーザ名">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="website" class="font-weight-bold">サイト</label>
                        <input type="text" name="website" class="form-control @error('website') is-invalid @enderror" id="website" value="{{ old('website') ?? $user->website }}" placeholder="https://guildoh.com/">
                    </div>
                </div>

                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('profile_setup_step2') }}" class="btn-common cancel mr-3 text-decoration-none">
                        前へ
                    </a>
                    <button type="submit" class="btn-common orange float-right">
                        次へ
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
