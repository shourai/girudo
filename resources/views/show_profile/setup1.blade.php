<?php $isInSetup = true; ?>

@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'user', 'title' => 'プロフィール登録'])

    <?php $user = Auth::user() ?>

    <div class="container-fluid pt-5 px-md-10">
        <div class="card">
            <div class="card-header nav-wizard-tab">
                <div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                    <a class="nav-item nav-link active">
                        <div class="wizard-step-icon">1</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">プロフィール</div>
                            <div class="wizard-step-text-details">基本情報</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link nav-wizard-next">
                        <div class="wizard-step-icon">2</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">スキル</div>
                            <div class="wizard-step-text-details">スキル情報</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link nav-wizard-next">
                        <div class="wizard-step-icon">3</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">SNS</div>
                            <div class="wizard-step-text-details">Twitter, Github, LinkedIn</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="progress wizard-bar">
                <div class="progress-bar wizard-17" role="progressbar" style="width: 17%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-none" role="progressbar" style="width: 83%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <form method="post" action="{{ route('profile_setup_step2') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body mt-4">
                    <h3 class="wizard-title">ステップ１</h3>
                    <h5 class="wizard-sub-title">プロフィール情報の入力</h5>
                    <div class="form-group">
                        <label for="name" class="font-weight-bold">名前</label>
                        <input name="name" class="form-control @error('name') is-invalid @enderror" id="name" type="text" value="{{ old('name') ?? $user->name }}" required autocomplete="name" autofocus>
                    </div>

                    <div class="form-group" id="SNS">
                        <label for="bio" class="font-weight-bold">自己紹介</label>
                        <textarea name="bio" class="form-control @error('bio') is-invalid @enderror" id="bio" rows="3">{{ old('bio') ?? $user->bio }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="profile-pic" class="font-weight-bold">写真</label>
                        <!--input type="file" name="profile_pic" id="profile-pic" class="form-control @error('profile_pic') is-invalid @enderror"-->

                        <div class="input-group mb-3 @error('profile_pic') is-invalid @enderror">
                            <div class="custom-file">
                                <input accept="image/*" type="file" name="profile_pic" id="profile-pic" class="custom-file-input form-control @error('profile_pic') is-invalid @enderror">
                                <label class="custom-file-label font-weight-bold" >ファイルを選択</label>
                            </div>
                        </div>

                        @if ($errors->has('profile_pic'))
                        <div class="invalid-feedback">
                            {{ $errors->first('profile_pic') }}
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="availability" class="font-weight-bold">ステータス</label>
                        <select class="form-control @error('availability') is-invalid @enderror" id="availability" name="availability">
                            <option value="1" {{ (old('availability') ?? $user->availability) == '1' ? 'selected' : '' }}>リソース余裕あり</option>
                            <option value="2" {{ (old('availability') ?? $user->availability) == '2' ? 'selected' : '' }}>リソース余裕なし</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <button type="submit" class="btn-common orange float-right">
                        次へ
                    </button>
                </div>
                <div class="card-header nav-wizard-tab nav-wizard-xl">
                    <div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                        <a class="nav-item nav-link">
                            <div class="wizard-step-icon">2</div>
                            <div class="wizard-step-text">
                                <div class="wizard-step-text-name">スキル</div>
                                <div class="wizard-step-text-details">スキル情報</div>
                            </div>
                        </a>
                        <a class="nav-item nav-link">
                            <div class="wizard-step-icon">3</div>
                            <div class="wizard-step-text">
                                <div class="wizard-step-text-name">SNS</div>
                                <div class="wizard-step-text-details">Twitter, Github, LinkedIn</div>
                            </div>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
