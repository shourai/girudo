@extends('layouts.app')

@section('content')
@include('components.content_top_block', ['icon' => 'user', 'title' => 'プロフィール', 'test' => '削除' ])
<div class="container-fluid pt-5">
    <div class="container">
        <div class="card">
            <div class="card-header">アカウントを削除</div>
            <div class="card-body">
                <p>本当にギルドーのアカウントを削除しますか？</p>
            </div>
            <div class="card-footer">
                <a href="/users/{{ Auth::id() }}" class="btn-common cancel mr-3 text-decoration-none">
                    キャンセル
                </a>
                <button type="button" class="btn-common orange float-right" onclick="event.preventDefault(); document.getElementById('delete-user-form').submit();">
                    削除
                </button>
            </div>
        </div>
    </div>
</div>

<form method="post" id="delete-user-form" action="/users/{{ Auth::id() }}/delete" style="deplay: none;">
    @csrf
</form>

@endsection
