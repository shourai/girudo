<?php $isInSetup = true; ?>

@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'user', 'title' => 'プロフィール登録'])

    <?php $user = Auth::user() ?>

    <div class="container-fluid pt-5 px-md-10">
        <div class="card">
            <div class="card-header nav-wizard-tab">
                <div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                    <a class="nav-item nav-link" href="{{ route('profile_setup_start') }}">
                        <div class="wizard-step-icon">1</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">プロフィール</div>
                            <div class="wizard-step-text-details">基本情報</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link active">
                        <div class="wizard-step-icon">2</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">スキル</div>
                            <div class="wizard-step-text-details">スキル情報</div>
                        </div>
                    </a>
                    <a class="nav-item nav-link nav-wizard-next">
                        <div class="wizard-step-icon">3</div>
                        <div class="wizard-step-text">
                            <div class="wizard-step-text-name">SNS</div>
                            <div class="wizard-step-text-details">Twitter, Github, LinkedIn</div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="progress wizard-bar">
                <div class="progress-bar wizard-33" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-33-66" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar wizard-none" role="progressbar" style="width: calc(33.33% - 0.3em)" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <form method="post" action="{{ route('profile_setup_step3') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body mt-4">
                    <h3 class="wizard-title">ステップ2</h3>
                    <h5 class="wizard-sub-title">スキル情報の入力</h5>
                    <div id="skill-form">
<?php
$skills = [];
if (old('skill')) {
    $skills = old('skill');
} else {
    foreach ($user->skills as $skill) {
        $skills[] = $skill->name;
    }
}
?>
                        @if ($skills)
                        @foreach ($skills as $i => $skill)
                        <div class="form-row" id="skill-row-{{ $i }}">
                            <div class="col-12">
                                <label for="skill-name-{{ $i }}" class="font-weight-bold">スキル</label>
                                <div class="input-group mb-2">
                                    <input type="text" id="skill-name-{{ $i }}" name="skill[]" class="form-control @error('skill.'.$i) is-invalid @enderror" value="{{ $skill }}">
                                    <div class="input-group-append">
                                        <button type="button" class="btn-common btn-close btn-skill-remove" onclick="removeSkill(event, {{ $i }})">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('skill.'.$i))
                            <div class="invalid-feedback">
                                {{ $errors->first('skill.'.$i) }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group" id="skill-level-row-{{ $i }}">
                            <label for="skill-level-{{ $i }}" class="font-weight-bold">スキルレベル</label>
                            <select class="form-control @error('skill_level.'.$i) is-invalid @enderror" id="skill-level-{{ $i }}" name="skill_level[]" >
                                <option value="1" {{ old('skill_level.'.$i) == '1' ? 'selected' : '' }}>初心者</option>
                                <option value="2" {{ old('skill_level.'.$i) == '2' ? 'selected' : '' }}>中級者</option>
                                <option value="3" {{ old('skill_level.'.$i) == '3' ? 'selected' : '' }}>エキスパート</option>
                            </select>
                            @if ($errors->has('skill_level.'.$i))
                            <div class="invalid-feedback">
                                {{ $errors->first('skill_level.'.$i) }}
                            </div>
                            @endif
                        </div>
                        <hr>
                        @endforeach
                        @else
                        <div class="form-row" id="skill-row-0">
                            <div class="col-12">
                                <label for="skill-name-0" class="font-weight-bold">スキル</label>
                                <div class="input-group mb-2">
                                    <input type="text" id="skill-name-0" name="skill[]" class="form-control" value="">
                                    <div class="input-group-append">
                                        <button type="button" class="btn-common btn-close btn-skill-remove" onclick="removeSkill(event, 0)">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="skill-level-row-0">
                            <label for="skill-level-0">スキルレベル</label>
                            <select class="form-control" id="skill-level-0" name="skill_level[]" >
                                <option value="1">初心者</option>
                                <option value="2">中級者</option>
                                <option value="3">エキスパート</option>
                            </select>
                        </div>
                        <hr>
                        @endif
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn-common outline-orange" onclick="addSkill(event)">スキル追加</button>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a href="{{ route('profile_setup_start') }}" class="btn-common cancel mr-3 text-decoration-none">
                        前へ
                    </a>
                    <button type="submit" class="btn-common orange float-right">
                        次へ
                    </button>
                </div>
                <div class="card-header nav-wizard-tab nav-wizard-xl">
                    <div class="nav nav-pills nav-justified flex-column flex-xl-row nav-wizard">
                        <a class="nav-item nav-link">
                            <div class="wizard-step-icon">3</div>
                            <div class="wizard-step-text">
                                <div class="wizard-step-text-name">SNS</div>
                                <div class="wizard-step-text-details">Twitter, Github, LinkedIn</div>
                            </div>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>

<script id="skill-form-template" type="text/mustache">
    <div class="form-row" id="skill-row-<% skill_idx %>">
        <div class="col-12">
            <label for="skill-name-<% skill_idx %>" class="font-weight-bold">スキル</label>
            <div class="input-group mb-2">
                <input type="text" id="skill-name-<% skill_idx %>" name="skill[]" class="form-control" value="">
                <div class="input-group-append">
                    <button type="button" class="btn-common btn-close btn-skill-remove" onclick="removeSkill(event, <% skill_idx %>)">@include('components.icons.useSprite', ['name' => 'cross'])</button>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group" id="skill-level-row-<% skill_idx %>">
        <label for="skill-level-<% skill_idx %>" class="font-weight-bold">スキルレベル</label>
        <select class="form-control" id="skill-level-<% skill_idx %>" name="skill_level[]" >
            <option value="1">初心者</option>
            <option value="2">中級者</option>
            <option value="3">エキスパート</option>
        </select>
    </div>
    <hr>
</script>

<script>
    var skill_idx = {{ old('skill') && sizeof(old('skill')) ? sizeof(old('skill')) : '0' }};
    function addSkill(e) {
        e.preventDefault();
        var div = document.createElement('div');
        div.innerHTML = Mustache.render(document.getElementById('skill-form-template').innerHTML, {skill_idx: skill_idx}).trim();
        document.getElementById('skill-form').appendChild(div.children[0]);
        document.getElementById('skill-form').appendChild(div.children[0]);
        skill_idx++;
    }

    function removeSkill(e, idx) {
        e.preventDefault();
        document.getElementById('skill-row-' + idx).remove();
        document.getElementById('skill-level-row-' + idx).remove();
    }
</script>
@endsection
