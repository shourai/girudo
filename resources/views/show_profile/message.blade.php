@extends('layouts.app')

@section('mainstyle') class="d-flex flex-column" @endsection

@section('content')
@include('components.content_top_block', ['icon' => 'mail', 'title' => $user->name, 'text' => 'メッセージ'])
<div class="chat-container">
    @include('components.chat', [
        'user_messages' => $user_messages,
        'messages' => $messages,
        'user_id' => $user_id,
        'initiator_id' => $initiator_id
    ])
</div>
@endsection
