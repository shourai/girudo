<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="フリーランス,エンジニア,クラウドソーシング,プログラマー">
    <meta name="description" content="ギルドーはフリーランスエンジニアのためのクラウドソーシングです。">
    <link rel="icon" type="image/x-icon" href="/images/favicon.ico">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" rel="stylesheet" />
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @includeWhen(env('GA_ENABLE'), 'google_analytics/google_analytics')
</head>
<body class="sb-nav-fixed">
    @include('components.icons.sprite')
    <nav class="sb-topnav navbar navbar-expand">
        <button class="btn btn-icon mr-lg-2 d-flex d-lg-none" id="sidebarToggle" href="#">
            <i data-feather="menu"></i>
        </button>
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ asset('images/Guildoh-logo-white.png') }}" class="img-fluid h-auto">
        </a>
        @auth
        <ul class="navbar-nav align-items-center ml-auto">
            <li class="nav-item dropdown no-caret mr-3 sb-dropdown-user">
                <a class="btn sb-btn-icon dropdown-toggle" id="navbarDropdownUserImage" href="#" role="button" data-toggle="dropdown" @if (!Auth::user()->profile_pic) style="border: 1px solid #ccc" @endif>
                    @if (Auth::user()->profile_pic)
                    <img class="img-fluid" src="{{ Auth::user()->profile_pic ? '/images/'.Auth::user()->profile_pic : 'https://source.unsplash.com/QAB-WJcbgJk/60x60' }}"/>
                    @else
                    {{ strtoupper(Auth::user()->name[0]) }}
                    @endif
                </a>
                <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownUserImage">
                    <a class="dropdown-item" href="{{ route('settings') }}">
                        <div class="sb-dropdown-item-icon">
                            <i data-feather="settings"></i>
                        </div>
                        設定
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        <div class="sb-dropdown-item-icon">
                            <i data-feather="log-out"></i>
                        </div>
                        ログアウト
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
        @endauth
    </nav>

    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            @if (!Auth::user()->completed_initial_project)
            <div class="sb-sidenav-account">
                <div class="row">
                    <div class="col-12 p-4">
                        <div class="acoount_title">アカウント力:</div>
                        <div class="acoount_strength">
                            @if (!Auth::user()->bio)
                                <span class="text-red">初級</span>
                            @elseif (!sizeof(Auth::user()->skills))
                                <span class="text-orange">中級</span>
                            @elseif (!Auth::user()->website)
                                <span class="text-blue">上級</span>
                            @elseif (!Auth::user()->completed_initial_project)
                                <span class="text-teal">エキスパート</span>
                            @endif
                        </div>
                        <div class="progress account-progress">
                            @if (!Auth::user()->bio)
                                <div class="progress-bar progress-bar-beginner" role="progressbar" style="width: 20%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar progress-bar-other" role="progressbar" style="width: 80%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            @elseif (!sizeof(Auth::user()->skills))
                                <div class="progress-bar progress-bar-intermediate" role="progressbar" style="width: 50%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar progress-bar-other" role="progressbar" style="width: 50%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            @elseif (!Auth::user()->website)
                                <div class="progress-bar progress-bar-advanced" role="progressbar" style="width: 70%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar progress-bar-other" role="progressbar" style="width: 30%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            @elseif (!Auth::user()->completed_initial_project)
                                <div class="progress-bar progress-bar-expert" role="progressbar" style="width: 90%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar progress-bar-other" role="progressbar" style="width: 10%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            @endif
                            {{-- for completely full!!
                                <div class="progress-bar progress-bar-full" role="progressbar" style="width: 100%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            --}}
                        </div>
                        <div class="account-shortage">
                            @if (!Auth::user()->bio)
                                プロフィール：<a href="{{ route('profile_setup_start') }}">自己紹介文を入力</a>
                            @elseif (!sizeof(Auth::user()->skills))
                                プロフィール：<a href="{{ route('profile_setup_step2') }}">スキルを追加</a>
                            @elseif (!Auth::user()->twitter && !Auth::user()->github && !Auth::user()->linkedin && !Auth::user()->website)
                                プロフィール：<a href="{{ route('profile_setup_step3') }}">SNSを追加</a>
                            @endif
                        </div>
                        <div class="account-shortage">
                            @if (!isset($isInCreateProject) || !isset($name))
                                @if (!isset($isInCreateProject) || !isset($name))
                                プロジェクト：<a href="{{ route('project_setup_start') }}">プロジェクトを追加</a>
                                @else
                                プロジェクト：<a href="{{ route('project_setup_start') }}" onclick="event.preventDefault(); document.getElementById('project-shortage-form').submit()">プロジェクトを追加</a>
                                <form method="post" action="{{ route('project_setup_start') }}" id="project-shortage-form">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="name" value="{{ $name }}" />
                                    <input type="hidden" name="description" value="{{ $description }}" />
                                    <input type="hidden" name="category" value="{{ $category }}" />
                                    @if ($skills)
                                    @foreach ($skill as $i => $s)
                                    <input type="hidden" name="skill[]" value="{{ $s }}" />
                                    @endforeach
                                    @endif
                                </form>
                                @endif
                            @elseif (!isset($skill))
                                プロジェクト：<a href="{{ route('project_setup_step2') }}" onclick="event.preventDefault(); document.getElementById('project-shortage-form').submit()">スキルを追加</a>
                                <form method="post" action="{{ route('project_setup_step2') }}" id="project-shortage-form">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="name" value="{{ $name }}" />
                                    <input type="hidden" name="description" value="{{ $description }}" />
                                    <input type="hidden" name="category" value="{{ $category }}" />
                                    @if ($skill)
                                    @foreach ($skill as $i => $s)
                                    <input type="hidden" name="skill[]" value="{{ $s }}" />
                                    @endforeach
                                    @endif
                                </form>
                            @else
                                プロジェクト：<a href="{{ route('project_setup_step3') }}" onclick="event.preventDefault(); document.getElementById('project-shortage-form').submit()">予算を追加</a>
                                <form method="post" action="{{ route('project_setup_step3') }}" id="project-shortage-form">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="name" value="{{ $name }}" />
                                    <input type="hidden" name="description" value="{{ $description }}" />
                                    <input type="hidden" name="category" value="{{ $category }}" />
                                    @foreach ($skill as $i => $s)
                                    <input type="hidden" name="skill[]" value="{{ $s }}" />
                                    @endforeach
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <nav class="sb-sidenav sb-shadow-right bg-sidebar {{ !Auth::user()->completed_initial_project ? 'pt-0' : '' }}">
                <div class="sb-sidenav-menu">
                    <div class="nav accordion" id="accordionSidenav">
                        @auth
                        <a class="nav-link" href="/home">
                            @include('components.icons.useSprite', ['name' => 'home'])
                            トップ
                        </a>
                        <a class="nav-link" href="/freelancers">
                            @include('components.icons.useSprite', ['name' => 'user'])
                            リソースに余裕ある人
                        </a>
                        <a class="nav-link" href="{{ route('groups') }}">
                            @include('components.icons.useSprite', ['name' => 'group'])
                            グループ
                        </a>
                        <a class="nav-link" href="{{ route('messages') }}">
                            @include('components.icons.useSprite', ['name' => 'mail'])
                            メッセージ
                        </a>
                        <a class="nav-link" href="/users/{{ Auth::id() }}">
                            @include('components.icons.useSprite', ['name' => 'user'])
                            プロフィール
                        </a>
                        <a class="nav-link" href="{{ route('create_invitation_url.index') }}">
                            @include('components.icons.useSprite', ['name' => 'compass'])
                            招待URLを生成
                        </a>
                        @endauth
                        @guest
                        <a class="nav-link" href="/"><div class="sb-nav-link-icon"><i data-feather="home"></i></div>トップ</a>
                        @endguest
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main @section('mainstyle') @show>
                @yield('content')
            </main>

        </div>

    </div>
    <footer class="sb-footer py-4 mt-auto sb-footer-light">
        <div class="container-fluid footer-container">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="footer-text small">&copy; Calano.Inc All rights reserved.</div>
                <div class="footer-text small">
                    <a href="/privacy_policy">プライバシーポリシー</a>
                    &middot;
                    <a href="/rule">ご利用規約</a>
                </div>
            </div>
        </div>
    </footer>
    <script src="{{ mix('/pubic/js/salvattore.min.js') }}" defer></script>

    @if (session('wizard') == 'project')
        <div class="modal fade" id="project-wizard-modal">
            <div class="modal-dialog">
                <div class="modal-common">
                    <div class="modal-header modal-header-common d-flex flex-column mt-4">
                        <h3 class="modal-common-title text-orange">あと少し！</h3>
                    </div>
                    <div class="modal-body modal-body-common text-center">
                        <h4 class="small-message">最初のプロジェクトを作って、<br>最初のギルドーを作りましょう！</h4>
                        <img class="m-5 bounce-img" src="/images/lightbulb.gif" />
                        <div>
                            <a class="btn-common orange text-decoration-none" href="{{ route('project_setup_start') }}">
                                プロジェクトを作る
                            </a>
                        </div>
                        <div class="mt-2 mb-4">
                            <a href="#"  class="cancel" data-toggle="modal" data-target="#project-wizard-modal">
                                あとで
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('#project-wizard-modal').modal('show');
    });
</script>
    @elseif (session('wizard') == 'complete')
        <div class="modal fade" id="complete-wizard-modal">
            <div class="modal-dialog">
                <div class="modal-common">
                    <div class="modal-header modal-header-common d-flex flex-column mt-4">
                        <h3 class="modal-common-title orange">おめでとうございます！</h3>
                    </div>
                    <div class="modal-body modal-body-common text-center">
                        <h4 class="midium-message">プロフィールレベルは<span class="emphasize-message h2">エキスパート</span>になりました！</h4>
                        <img class="m-3 bounce-img" src="/images/shield.gif" />
                        <h4 class="small-message mb-4">自分のギルドーを作りましょう！</h4>
                        <div>
                            <a class="btn-common orange text-decoration-none" href="/freelancers">
                                メンバー探しましょう！
                            </a>
                        </div>
                        <div class="mt-2 mb-4">
                            <a href="#"  class="cancel" data-toggle="modal" data-target="#project-wizard-modal">
                                ホームに戻る
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('#complete-wizard-modal').modal('show');
    });
</script>
    @endif
</body>
</html>
