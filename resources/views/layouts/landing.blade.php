<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="keywords" content="フリーランス,エンジニア,クラウドソーシング,プログラマー">
    <meta name="description" content="ギルドーはフリーランスエンジニアのためのクラウドソーシングです。">
    <link rel="icon" type="image/x-icon" href="/images/favicon.ico">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery.matchHeight-min.js') }}" defer></script>
    <script src="{{ asset('js/slick.min.js') }}" defer></script>
    <script src="{{ asset('js/custom.js') }}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" rel="stylesheet" />
    <link href="{{ asset('css/landing.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    @includeWhen(env('GA_ENABLE'), 'google_analytics/google_analytics')
</head>
<body>
    <div id="layoutDefault">
        <div id="layoutDefault_content">
            <main>
                <nav class="navbar navbar-marketing navbar-expand navbar-dark guil-head">
                    <div class="container">
                        <a class="navbar-brand text-white" href="{{ url('/') }}">
                            <img src="{{ asset('images/Guildoh-logo-white.png') }}" class="img-fluid">
                        </a>
                        <div id="navbarSupportedContent" class="collapse navbar-collapse">
                            <ul class="navbar-nav navbar-nav-left ml-lg-4 d-none d-md-flex">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('https://calano.jp/#company') }}" target="_blank">会社概要</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ url('https://calano.jp/#contact') }}" target="_blank">お問い合わせ</a>
                                </li>
                            </ul>
                            <ul class="navbar-nav navbar-nav-right mr-lg-4">
                                @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">ログイン</a>
                                </li>
                                @else
                                <li class="nav-item dropdown dropdown-sm no-caret">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="{{ route('home') }}" role="button" {{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre--}}>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    {{--
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item py-3" href="/users/{{ Auth::id() }}">
                                            <div class="dropdown-item-icon">
                                                <i data-feather="user"></i>
                                            </div>
                                            プロフィール
                                        </a>
                                        <a class="dropdown-item py-3" href="{{ route('messages') }}">メッセージ</a>
                                        <a class="dropdown-item py-3" href="{{ route('settings') }}">
                                            <div class="dropdown-item-icon">
                                                <i data-feather="settings"></i>
                                            </div>
                                            設定
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item py-3" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <div class="dropdown-item-icon">
                                                <i data-feather="log-out"></i>
                                            </div>
                                            ログアウト
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                    --}}
                                </li>
                                @endif
                            </ul>
                        </div>
                        @if (Route::has('register'))
                        <div class="guil-signup">
                            <a href="{{ route('unable_register') }}"><button class="btn signup">会員登録（無料）</button></a>
                        </div>
                        @endif

                    </div>
                </nav>

                @yield('content')
            </main>
        </div>
        <div id="layoutDefault_footer">
            <footer class="footer mt-autok">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-6 small"><span class="black-text">&copy; Calano.Inc All rights reserved.</span></div>
                        <div class="col-md-6 text-md-right small">
                            <a href="{{ url('https://calano.jp/#company') }}" target="_blank">会社概要</a>
                            &middot;
                            <a href="{{ url('https://calano.jp/#contact') }}" target="_blank">お問い合わせ</a>
                            &middot;
                            <a href="/privacy_policy">プライバシーポリシー</a>
                            &middot;
                            <a href="/rule">ご利用規約</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>
