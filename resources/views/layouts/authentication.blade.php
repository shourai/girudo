<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="フリーランス,エンジニア,クラウドソーシング,プログラマー">
    <meta name="description" content="ギルドーはフリーランスエンジニアのためのクラウドソーシングです。">
    <link rel="icon" type="image/x-icon" href="/images/favicon.ico">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" rel="stylesheet" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    @includeWhen(env('GA_ENABLE'), 'google_analytics/google_analytics')
</head>
<body>
    <div id="layoutAuthentication" class="guil-layout-auth">
        <div id="layoutAuthentication_content">
            <main>
                @yield('content')
            </main>
        </div>
        <div id="layoutAuthentication_footer">
            <div class="footer-color-change">
                <div class="right-section">
                <img src="{{ asset('images/guildoh-sprout-shadow.png') }}" class="img-fluid">
                </div>
            </div>
            <footer class="sb-footer mt-auto sb-footer-dark">
                <div class="container">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div><span class="black-text">&copy; Calano.Inc All rights reserved.</span></div>
                        <div>
                            <a href="{{ url('https://calano.jp/#company') }}" target="_blank">会社概要</a>
                            &middot;
                            <a href="{{ url('https://calano.jp/#contact') }}" target="_blank">お問い合わせ</a>
                            &middot;
                            <a href="/privacy_policy">プライバシーポリシー</a>
                            &middot;
                            <a href="/rule">ご利用規約</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
