@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'user', 'title' => '仲間を探そう！', 'text' => ''])
    <section class="container-fluid pt-5">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center flex-column">
                    <h5 class="info-project-title">リソースに余裕がある人一覧</h5>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ $available_freelancers->links() }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="grid d-flex justify-content-between" data-columns>
                    @foreach ($available_freelancers as $freelancer)
                        <div class="item">
                            <div class="info-freelancer mb-3">
                                <div
                                    class="member-title d-flex flex-sm-row justify-content-between align-items-center flex-column">
                                    <h5>
                                        <a href="{{ route('profile', ['user_id' => $freelancer->id]) }}">{{ $freelancer->name }}</a>
                                    </h5>
                                    <div class="d-flex">
                                        @auth
                                            @if ($freelancer->id != Auth::id())
                                                <div class="text-right mb-2">
                                                    <a href="{{ route('user_message', ['user_id' => $freelancer->id, 'initiator_id' => Auth::id()]) }}"
                                                       class="btn-common mail text-decoration-none">@include('components.icons.useSprite', ['name' => 'mail']) </a>
                                                </div>
                                            @endif

                                        @endauth
                                    </div>
                                </div>

                                <div style="background-image: url('{{ $freelancer->profile_pic ? '/images/'.$freelancer->profile_pic :  asset('/images/user.png') }}')"
                                    class="member-img"></div>
                                <div class="info-skills d-flex justify-content-start flex-wrap">
                                    @foreach ($freelancer->skills as $skill)
                                        <div class="info-skill-home">{{ $skill->name }}</div>
                                    @endforeach
                                </div>
                            </div>
                            @if (sizeof($freelancer->projects))
                                <div class="footer-freelancer d-flex justify-content-center" >
                                    <a class="btn btn-black btn-sm outline-none freelancer-projects-button"
                                       data-toggle="collapse" href="#projects-{{ $freelancer->id }}" role="button">プロジェクトリストを表示
                                        &nbsp;+</a>
                                </div>
                                <div id="projects-{{ $freelancer->id }}" class="collapse freelancer-projects">
                                    @foreach ($freelancer->projects as $project)
                                        <div class="item">
                                            <div class="project-content">
                                                <div class="d-flex flex-wrap justify-content-between">
                                                @if ($project->budget_hourly)
                                                    <p class="home-info-hour mb-1">{{ number_format($project->budget_hourly) }}円/最低時給</p>
                                                @endif
                                                @if ($project->budget_total)
                                                    <p class="home-info-hour mb-1">{{ number_format($project->budget_total) }}円/予算</p>
                                                @endif
                                                </div>
                                                @if ($project->project_status == '1')
                                                    <p class="home-info-time">
                                                        @if ($project->project_length == '1')
                                                            1ヶ月以内
                                                        @elseif ($project->project_length == '2')
                                                            1〜3ヶ月
                                                        @elseif ($project->project_length == '3')
                                                            3〜6ヶ月
                                                        @else
                                                            半年以上
                                                        @endif
                                                    </p>
                                                @endif
                                                <p class="info-description">{{ $project->getDescription() }}</p>
                                                <p class="info-skills">
                                                @foreach ($project->skills as $skill)
                                                    <div class="info-skill-home">{{ $skill->name }}</div>
                                                    @endforeach</p>
                                            </div>

                                            <div
                                                class="main-project-footer d-flex flex-column justify-content-center align-items-center">
                                                <h6><a class="project-name"
                                                       href="{{ route('project', ['project_id' => $project->id]) }}">{{ $project->name }}</a>
                                                </h6>
                                                @if ($project->category_id)
                                                    <p class="category-project"> {{ $project->category->name }}</p>
                                                @endif
                                                <hr>
                                                @auth
                                                    @if (Auth::id() != $project->owner_id)
                                                        <a href="{{ route('project', ['project_id' => $project->id]) }}#project-chat"
                                                           class="btn-common mail-text">@include('components.icons.useSprite', ['name' => 'mail'])
                                                            メッセージ</a>
                                                    @endif
                                                @endauth
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="d-flex justify-content-center pb-3">
                                        <a class="btn btn-orange btn-sm outline-none freelancer-projects-button pb-3 pt-3"
                                           data-toggle="collapse" href="#projects-{{ $freelancer->id }}" role="button">プロジェクトリストを閉じる -</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ $available_freelancers->links() }}
            </div>
        </div>

    </section>
@endsection
