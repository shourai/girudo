@extends('layouts.authentication')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class=" mt-5">
            <div class="logo">
                <img src="{{ asset('images/Logo.png') }}" class="img-fluid">
            </div>
            <div class="card shadow-lg border-0 rounded-lg guil-login-block">
                <div class="card-header justify-content-center">
                    <h2>注意</h2>
                </div>

                <div class="card-body">
                    <div class="form-group">
                        <label>
                            現在Guildohは<b>招待制</b>を採用しています。<br>
                            お知り合いから頂いた招待URLからアクセスしてアカウント登録お願いします。
                        </label>
                    </div>

                    <div class="form-group text-center mt-4 mb-0">
                        <a href="{{ url('https://calano.jp/#contact') }}" target="_blank" class="btn button1">お問い合わせ</a>
                    </div>
                </div>

                <div class=" card-footer pt-0  text-center">
                    <div class="small orange">
                        <a href="{{ url('/') }}">ホームページに戻る</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
