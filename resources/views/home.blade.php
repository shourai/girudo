@extends('layouts.app')

@section('content')
    @include('components.content_top_block', ['icon' => 'home', 'title' => 'トップ', 'text' => 'ギルドーへようこそ！'])
    <section class="container-fluid pt-5">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex flex-sm-row justify-content-between align-items-center flex-column">
                    <h5 class="info-project-title">新着募集プロジェクト</h5>

                    <div>
                        <a class="btn-common orange text-decoration-none"
                           href="{{ route('new_project') }}">+ プロジェクトを追加</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ $recruiting_projects->links() }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="grid d-flex justify-content-between" data-columns>
                    @foreach ($recruiting_projects as $project)
                        <div class="item">
                            <div class="project-content">
                                <div class="d-flex flex-wrap justify-content-between">
                                @if ($project->budget_hourly)
                                    <p class="home-info-hour mb-1">{{ number_format($project->budget_hourly) }}円/最低時給</p>
                                @endif
                                @if ($project->budget_total)
                                    <p class="home-info-hour mb-1">{{ number_format($project->budget_total) }}円/予算</p>
                                @endif
                                </div>
                                @if ($project->project_status == '1')
                                    <p class="home-info-time">
                                        @if ($project->project_length == '1')
                                            1ヶ月以内
                                        @elseif ($project->project_length == '2')
                                            1〜3ヶ月
                                        @elseif ($project->project_length == '3')
                                            3〜6ヶ月
                                        @else
                                            半年以上
                                        @endif
                                    </p>
                                @endif
                                <p class="info-description">{{ $project->getDescription() }}</p>
                                <div class="info-skills">
                                @foreach ($project->skills as $skill)
                                    <div class="info-skill-home">{{ $skill->name }}</div>
                                @endforeach
                                </div>
                            </div>

                            <div
                                class="main-project-footer d-flex flex-column justify-content-center align-items-center">
                                <h6><a class="project-name"
                                       href="{{ route('project', ['project_id' => $project->id]) }}">{{ $project->name }}</a>
                                </h6>
                                @if ($project->category_id)
                                    <p class="category-project"> {{ $project->category->name }}</p>
                                @endif
                                @if ($project->group)
                                    <p>{{ $project->group->name }}</p>
                                @endif
                                <hr>
                                @auth
                                    @if (Auth::id() != $project->owner_id)
                                        <a href="{{ route('project', ['project_id' => $project->id]) }}#project-chat"
                                           class="btn-common mail-text">@include('components.icons.useSprite', ['name' => 'mail'])
                                            メッセージ</a>
                                    @endif
                                @endauth
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ $recruiting_projects->links() }}
            </div>
        </div>

    </section>

    @if (!Auth::user()->shown_initial_wizard)
        <?php Auth::user()->hasShownWizard() ?>
        <div class="modal fade" id="initial-wizard-modal">
            <div class="modal-dialog">
                <div class="modal-common">
                    <div class="modal-body modal-body-common text-center">
                        <h4 class="midium-message"><span>ようこそ！</span><br/><span class="text-orange h5">まずは、プロフィールを入力してみよう！</span></h4>
                        <h4 class="midium-message">現在のプロフィールレベルは
                            <span class="text-red d-block h2">@if (!Auth::user()->bio)
                                    初級
                                @elseif (!sizeof(Auth::user()->skills))
                                    中級
                                @elseif (!Auth::user()->website)
                                    上級
                                @elseif (!Auth::user()->completed_initial_project)
                                    エキスパート
                                @endif</span></h4>
                        <img class="mt-4 mb-5 bounce-img" src="/images/user_1.svg" />
                        <h4 class="small-message mb-4">プロフィールをレベルアップしましょう！</h4>
                        <div class="mb-2">
                            <a class="btn-common orange text-decoration-none" href="{{ route('profile_setup_start') }}">
                                はじめる
                            </a>
                        </div>
                        <div class="mb-4">
                            <a href="#" class="cancel" data-toggle="modal" data-target="#initial-wizard-modal">
                                あとで
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('#initial-wizard-modal').modal('show');
    });
</script>
    @endif
@endsection
