@extends('layouts.app')

@section('content')
@include('components.content_top_block', ['icon' => 'mail', 'title' => 'メッセージ', 'text' => '今日は素晴らしいことをしましょう！'])

<section class="message-list">
    <h4 class="message-list-title">メッセージ一覧</h4>
    @forelse ($user_messages as $message)
    <div class="message-card">
        <div class="message-card-body">
            <a href="{{ route('user_message', ['user_id' => $message->user_id_smaller, 'initiator_id' => $message->user_id_larger]) }}">
                @if ($message->userSmaller->id == Auth::id())
                <h5>{{ $message->userLarger->name }}</h5>
                @else
                <h5>{{ $message->userSmaller->name }}</h5>
                @endif
                <pre>{{ $message->message }}</pre>
            </a>
        </div>
    </div>
    @empty
        <p>なし</p>
    @endforelse
</section>
@endsection
