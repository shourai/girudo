<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMessage extends Model {
    protected $fillable = ['user_id_smaller', 'user_id_larger', 'send_id', 'message'];

    public function author() {
        return $this->belongsTo('App\User', 'send_id');
    }

    public function userSmaller() {
        return $this->belongsTo('App\User', 'user_id_smaller');
    }

    public function userLarger() {
        return $this->belongsTo('App\User', 'user_id_larger');
    }
}
