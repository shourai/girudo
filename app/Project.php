<?php

namespace App;

use Illuminate\Support\HtmlString;
use Illuminate\Database\Eloquent\Model;
use League\CommonMark\CommonMarkConverter;

class Project extends Model {
    protected $fillable = [
        'name', 'description', 'budget_hourly', 'budget_total', 'project_length',
        'project_status', 'owner_id', 'category_id', 'privacy',
    ];

    public function owner() {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function skills() {
        return $this->belongsToMany('App\Skill');
    }

    public function messages() {
        return $this->hasMany('App\ProjectMessage');
    }

    public function category() {
        return $this->belongsTo('App\ProjectCategory', 'category_id');
    }

    public function members() {
        return $this->belongsToMany('App\User', 'project_members');
    }

    public function group() {
        return $this->belongsTo('App\Group');
    }

    public function getDescription() {
        $converter = new CommonMarkConverter([
            'allow_unsafe_links' => false,
            'html_input' => 'escape',
        ]);
        return new HtmlString($converter->convertToHtml($this->description));
    }

    public function projectInvitation() {


    }
}
