<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewProjectMessage extends Notification {
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($project_id, $initiator_id, $message) {
        $this->project_id = $project_id;
        $this->initiator_id = $initiator_id;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        $url = url('/projects/'.$this->project_id.'/?initiator_id='.$this->initiator_id);

        return (new MailMessage)
                    ->subject('募集しているプロジェクトにメッセージが届きました')
                    ->line('あなたの募集しているプロジェクトにメッセージが届きました')
                    ->action('内容を確認する', $url)
                    ->line('メッセージ内容:')
                    ->line($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
