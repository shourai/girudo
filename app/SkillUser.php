<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class SkillUser extends Pivot {
    public function levelStr() {
        if ($this->level == 1) {
            return '初心者';
        } elseif ($this->level == 2) {
            return '中級者';
        }
        return 'エキスパート';
    }
}
