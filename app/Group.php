<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\HtmlString;
use League\CommonMark\CommonMarkConverter;
use Illuminate\Support\Facades\DB;

class Group extends Model {
    protected $table = 'groups';

    protected $fillable = ['name', 'description', 'privacy'];

    public function isAdmin($userId) {
        return $this->owners()->where('user_id', $userId)->count() > 0 ? true : false;
    }

    public function owners() {
        return $this->belongsToMany('App\User')->wherePivot('role', 1);
    }

    public function members() {
        return $this->belongsToMany('App\User')->using('App\GroupUser')->withPivot(['role']);
    }

    public function projects() {
        return $this->hasMany('App\Project');
    }

    public function getDescription() {
        $converter = new CommonMarkConverter([
            'allow_unsafe_links' => false,
            'html_input' => 'escape',
        ]);
        return new HtmlString($converter->convertToHtml($this->description));
    }
}
