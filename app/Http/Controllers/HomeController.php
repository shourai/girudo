<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Project;
use App\User;
use App\ProjectMessage;
use App\UserMessage;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $recruiting_projects = Project::where('project_status', 1)->where('privacy', 1)->orderBy('created_at', 'desc')->paginate(20);
        return view('home', ['recruiting_projects' => $recruiting_projects]);
    }

    public function freelancers() {
        $available_freelancers = User::where('availability', 1)->orderBy('created_at', 'desc')->paginate(20);
        return view('freelancers', ['available_freelancers' => $available_freelancers]);
    }

    public function byebye() {
        return view('byebye');
    }

    public function welcome() {
        $recruiting_projects = Project::where('project_status', 1)->orderBy('created_at', 'desc')->take(5)->get();
        $available_freelancers = User::where('availability', 1)->orderBy('created_at', 'desc')->take(3)->get();
        return view('welcome', ['recruiting_projects' => $recruiting_projects, 'available_freelancers' => $available_freelancers]);
    }

    public function settings() {
        $user = Auth::user();

        return view('settings', ['user' => $user]);
    }

    public function saveSettings(Request $request) {
        $user = Auth::user();

        $v = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
            'notification_project_message' => 'boolean',
            'notification_user_message' => 'boolean',
        ]);
        $v->sometimes('new-password', 'required|string|min:8|confirmed', function($input) {
            return $input->password != '';
        });
        $v->validate();

        if ($request->get('password')) {
            if (!(Hash::check($request->get('password'), $user->password))) {
                return redirect()->back()->withErrors(['現在のパスワードが設定されているパスワードと一致していません']);
            }

            if (strcmp($request->get('password'), $request->get('new-password')) == 0) {
                return redirect()->back()->withErrors(['新しいパスワードが一致していません']);
            }

            $user->password = bcrypt($request->get('new-password'));
        }

        $user->email = $request->email;
        $user->notification_project_message = $request->notification_project_message ? true : false;
        $user->notification_user_message = $request->notification_user_message ? true : false;
        $user->save();

        return redirect()->back()->with('success', '変更が保存されました');
    }

    public function messages() {
        $latest_user_messages = DB::table('user_messages')
            ->select('user_id_smaller', 'user_id_larger', DB::raw('MAX(created_at) as last_message_date'))
            ->where('user_id_smaller', Auth::id())
            ->orWhere('user_id_larger', Auth::id())
            ->groupBy('user_id_smaller', 'user_id_larger');
        $user_messages = UserMessage::joinSub($latest_user_messages, 'latest_messages', function ($join) {
            $join->on([
                ['user_messages.user_id_smaller', '=', 'latest_messages.user_id_smaller'],
                ['user_messages.user_id_larger', '=', 'latest_messages.user_id_larger'],
                ['user_messages.created_at', '=', 'latest_messages.last_message_date'],
            ]);
        })->get();

        return view('messages', ['user_messages' => $user_messages]);
    }
}
