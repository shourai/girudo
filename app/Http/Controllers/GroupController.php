<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use App\Notifications\NewGroupMemberInvitationMessage;
use App\Group;
use App\User;

class GroupController extends Controller {
    public function new() {
        return view('group.new');
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'string|nullable',
            'privacy' => 'required|in:1,2',
        ]);
        if ($validator->fails()) {
            return redirect('/users/'.$userId)->withErrors($validator, 'group');
        }

        $group = new Group();
        $group->name = $request->name;
        $group->description = $request->description;
        $group->privacy = $request->privacy;
        $group->save();

        $group->members()->save(Auth::user(), ['role' => 1]);

        return redirect('/groups/'.$group->id)->with('success', 'グループを設立しました');
    }

    public function list() {
        $groups = Group::where('privacy', 1)->orderBy('created_at', 'DESC')->paginate(20);
        return view('group.list', ['groups' => $groups]);
    }

    public function show($group_id) {
        $group = Group::findOrFail($group_id);

        if ($group->privacy == 2) {
            // Private group
            if (!Auth::check() || $group->members()->where('user_id', Auth::id())->count() == 0) {
                return abort(403);
            }
        }

        return view('group.show', ['group' => $group]);
    }

    public function edit($group_id) {
        $group = Group::findOrFail($group_id);
        if (!$group->isAdmin(Auth::id())) {
            return abort(404);
        }

        return view('group.edit', ['group' => $group]);
    }

    public function update(Request $request, $group_id) {
        $group = Group::findOrFail($group_id);
        if (!$group->isAdmin(Auth::id())) {
            return abort(404);
        }

        $request->validate([
            'name' => 'required|string',
            'description' => 'string|nullable',
            'privacy' => 'required|in:1,2',
        ]);

        $group->name = $request->name;
        $group->description = $request->description;
        $group->privacy = $request->privacy;
        $group->save();

        return redirect()->route('group', ['group_id' => $group->id]);
    }

    public function delete($group_id) {
        $group = Group::findOrFail($group_id);
        if (!$group->isAdmin(Auth::id())) {
            return abort(404);
        }

        $group->delete();
        return redirect('home');
    }

    public function deleteMember(Request $request, $group_id) {
        $group = Group::findOrFail($group_id);
        if (!$group->isAdmin(Auth::id())) {
            return abort(404);
        }

        $request->validate([
            'member_id' => 'required',
        ]);
        $group->members()->detach($request->member_id);

        return redirect()->route('group', ['group_id' => $group_id])->with('success', 'メンバーから削除しました');
    }

    public function addMember(Request $request, $group_id) {
        $group = Group::findOrFail($group_id);
        if (!$group->isAdmin(Auth::id())) {
            return abort(404);
        }

        $request->validate([
            'member' => 'required',
        ]);

        $numberResults = User::where('email', $request->member)
            ->orWhere('name', 'like', '%'.$request->member.'%')
            ->count();

        if ($numberResults > 1) {
            return redirect()->route('group', ['group_id' => $group_id])->with('error', '検索結果が複数人に該当しました');
        } elseif ($numberResults == 0) {
            return redirect()->route('group', ['group_id' => $group_id])->with('error', '検索結果該当がありませんでした');
        }

        $member = User::where('email', $request->member)
            ->orWhere('name', 'like', '%'.$request->member.'%')
            ->first();

        // create url
        $url = URL::temporarySignedRoute('group_invitation', now()->addHours(24), ['user_id'=>$member->id,'group_id' => $group->id]);

        if ($numberResults == 1) {
            $member->notify(new NewGroupMemberInvitationMessage($member->id,$group,$url));
        }

        return redirect()->route('group', ['group_id' => $group_id])->with('success', '招待メールを送信しました。');
    }

    public function invitation(Request $request, $group_id) {
        if (!$request->hasValidSignature()) {
            abort(401);
        }
        $member = User::where('email', $request->user_id)
            ->orWhere('id', $request->user_id)
            ->first();

        $group = Group::findOrFail($request->group_id);
        $group->members()->save($member, ['role' => 0]);

        return redirect()->route('group', ['group_id' => $group_id])->with('success', 'メンバーに追加されました。');
    }

    public function exitUser(Request $request, $user_id) {
        $request->validate([
            'group_id' => 'required|exists:groups,id',
        ]);

        $group = Group::findOrFail($request->group_id);
        $group->members()->detach(Auth::id());

        return redirect('/users/'.$user_id)->with('success', 'グループを退出しました');
    }
}
