<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Team;
use App\User;
use App\Skill;
use App\ExtraProject;
use App\UserMessage;
use App\ProjectMessage;
use App\Notifications\NewUserMessage;
use App\Group;

use Log;

class ShowProfile extends Controller {
    public function __construct() {
    }

    public function index($userId) {
        $user = User::findOrFail($userId);

        return view('show_profile.index', ['user' => $user]);
    }

    public function edit($userId) {
        if ($userId != Auth::id()) {
            return abort(403);
        }

        return view('show_profile.edit', ['user' => Auth::user()]);
    }

    public function save(Request $request, $userId) {
        if ($userId != Auth::id()) {
            return abort(403);
        }

        $user = User::findOrFail($userId);

        $request->validate([
            'name' => 'required',
            'availability' => 'required|in:1,2',
            'bio' => 'string|nullable',
            'twitter' => 'string|nullable',
            'github' => 'string|nullable',
            'linkedin' => 'string|nullable',
            'website' => 'string|nullable',
            'profile_pic' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($request->profile_pic) {
            $imageName = Auth::id().'.'.time().'.'.$request->profile_pic->extension();

            $request->profile_pic->move(public_path('images'), $imageName);
            $user->profile_pic = $imageName;
        }

        $user->name = $request->name;
        $user->availability = $request->availability;
        $user->bio = $request->bio;
        $user->twitter = $request->twitter;
        $user->github = $request->github;
        $user->linkedin = $request->linkedin;
        $user->website = $request->website;
        $user->save();

        return redirect('/users/'.$userId)->with('success', 'プロフィールの変更が保存されました');
    }

    public function addSkill(Request $request, $userId) {
        if ($userId != Auth::id()) {
            return abort(403);
        }

        $user = User::findOrFail($userId);

        $validator = Validator::make($request->all(), [
            'skill' => 'required|string',
            'skill_level' => 'required|in:1,2,3',
        ]);
        if ($validator->fails()) {
            return redirect('/users/'.$userId)->withErrors($validator, 'skill');
        }

        $skill = Skill::where('name', $request->skill)->firstOr(function() use ($request) {
            $skill = new Skill();
            $skill->name = $request->skill;
            $skill->save();
            return $skill;
        });
        $user->skills()->save($skill, ['level' => $request->skill_level]);

        return redirect('/users/'.$userId)->with('success', 'スキルが追加されました');
    }

    public function deleteSkill(Request $request, $userId) {
        if ($userId != Auth::id()) {
            return abort(403);
        }

        $user = User::findOrFail($userId);
        $skill = Skill::where('name', $request->skill_id)->firstOrFail();
        $user->skills()->wherePivot('level', '=', $request->skill_level)->detach($skill);

        return redirect('/users/'.$userId)->with('success', 'スキルが削除されました');
    }

    public function addExtraProject(Request $request, $userId) {
        if ($userId != Auth::id()) {
            return abort(403);
        }

        $user = User::findOrFail($userId);

        $validator = Validator::make($request->all(), [
            'outline' => 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect('/users/'.$userId)->withErrors($validator, 'project');
        }

        $extraProject = ExtraProject::where('outline', $request->outline)->firstOr(function() use ($request,$user) {
            $extraProject = new ExtraProject();
            $extraProject->outline = $request->outline;
            $extraProject->user_id = $user->id;
            $extraProject->save();

            return $extraProject;
        });

        return redirect('/users/'.$userId)->with('success', 'プロジェクトが追加されました');
    }

    public function newGroup(Request $request, $userId) {
        if ($userId != Auth::id()) {
            return abort(403);
        }

        $user = User::findOrFail($userId);

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'string|nullable',
            'privacy' => 'required|in:1,2',
        ]);
        if ($validator->fails()) {
            return redirect('/users/'.$userId)->withErrors($validator, 'group');
        }

        $group = new Group();
        $group->name = $request->name;
        $group->description = $request->description;
        $group->privacy = $request->privacy;
        $group->save();

        $group->members()->save($user, ['role' => 1]);

        return redirect('/users/'.$userId)->with('success', 'グループを設立しました');
    }

    public function delete(Request $request, $userId) {
        if ($userId != Auth::id()) {
            return abort(403);
        }

        $user = User::findOrFail($userId);
        $user->delete();

        Auth::logout();
        return redirect('/byebye');
    }

    public function addMember($userId) {
        if ($userId != Auth::id()) {
            return abort(403);
        }

        $user = User::findOrFail($userId);

        return view('show_profile.add_member', ['user' => Auth::user()]);
    }

    public function saveMember(Request $request, $userId) {
        if ($userId != Auth::id()) {
            return abort(403);
        }

        $owner = User::findOrFail($userId);

        $team = Team::where('owner_id', $userId)->firstor(function() use ($userId) {
            $team = Team::create([
                'owner_id' => $userId,
            ]);

            return $team;
        });

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'skill.*' => 'string|nullable',
            'skill_level.*' => 'in:1,2,3|nullable',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $team->teamMembers()->save($user);
        $user->sendEmailVerificationNotification();

        if ($request->skill) {
            foreach ($request->skill as $i => $skill_name) {
                $level = $request->skill_level[$i];
                if ($skill_name && $level) {
                    $skill = Skill::where('name', $skill_name)->firstOr(function() use ($skill_name) {
                        $skill = new Skill();
                        $skill->name = $skill_name;
                        $skill->save();
                        return $skill;
                    });
                    $user->skills()->save($skill, ['level' => $level]);
                }
            }
        }

        return redirect('/users/'.$userId)->with('success', 'チームメンバーが追加されました');
    }

    public function message($user_id, $initiator_id) {
        if (Auth::id() != $user_id && Auth::id() != $initiator_id) {
            return abort(404);
        }
        if ($user_id == $initiator_id) {
            return abort(404);
        }
        if ($user_id < $initiator_id) {
            $user_id_smaller = $user_id;
            $user_id_larger = $initiator_id;
        } else {
            $user_id_smaller = $initiator_id;
            $user_id_larger = $user_id;
        }
        $messages = UserMessage::where([
            ['user_id_smaller', '=', $user_id_smaller],
            ['user_id_larger', '=', $user_id_larger],
        ])->orderBy('created_at', 'asc')->get();

        $latest_user_messages = DB::table('user_messages')
            ->select('user_id_smaller', 'user_id_larger', DB::raw('MAX(created_at) as last_message_date'))
            ->where('user_id_smaller', Auth::id())
            ->orWhere('user_id_larger', Auth::id())
            ->groupBy('user_id_smaller', 'user_id_larger');
        $user_messages = UserMessage::joinSub($latest_user_messages, 'latest_messages', function ($join) {
            $join->on([
                ['user_messages.user_id_smaller', '=', 'latest_messages.user_id_smaller'],
                ['user_messages.user_id_larger', '=', 'latest_messages.user_id_larger'],
                ['user_messages.created_at', '=', 'latest_messages.last_message_date'],
            ]);
        })->get();

        if (Auth::id() == $user_id_smaller) {
            $user = User::findOrFail($user_id_larger);
        } else {
            $user = User::findOrFail($user_id_smaller);
        }

        return view('show_profile.message', ['user' => $user, 'user_id' => $user_id, 'initiator_id' => $initiator_id, 'messages' => $messages, 'user_messages' => $user_messages]);
    }

    public function sendMessage(Request $request, $user_id, $initiator_id) {
        if (Auth::id() != $user_id && Auth::id() != $initiator_id) {
            return abort(404);
        }
        if ($user_id == $initiator_id) {
            return abort(404);
        }

        if ($user_id < $initiator_id) {
            $user_id_smaller = $user_id;
            $user_id_larger = $initiator_id;
        } else {
            $user_id_smaller = $initiator_id;
            $user_id_larger = $user_id;
        }

        $request->validate([
            'message' => 'string',
        ]);

        UserMessage::create([
            'user_id_smaller' => $user_id_smaller,
            'user_id_larger' => $user_id_larger,
            'send_id' => Auth::id(),
            'message' => $request->message,
        ]);

        if (Auth::id() == $user_id) {
            $user = User::findOrFail($initiator_id);
        } else {
            $user = User::findOrFail($user_id);
        }
        if ($user->notification_user_message) {
            $user->notify(new NewUserMessage($user_id, $initiator_id, $request->message));
        }

        return redirect()->back();
    }

    public function deleteConfirm($user_id) {
        if (Auth::id() != $user_id) {
            return abort(404);
        }
        return view('show_profile.delete');
    }

    public function setup1() {
        return view('show_profile.setup1');
    }

    public function setup2View() {
        return view('show_profile.setup2');
    }

    public function setup3View() {
        return view('show_profile.setup3');
    }

    public function setup2(Request $request) {
        $request->validate([
            'name' => 'required',
            'availability' => 'required|in:1,2',
            'bio' => 'string|nullable',
            'profile_pic' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $user = Auth::user();

        if ($request->profile_pic) {
            $imageName = Auth::id().'.'.time().'.'.$request->profile_pic->extension();

            $request->profile_pic->move(public_path('images'), $imageName);
            $user->profile_pic = $imageName;
        }

        $user->name = $request->name;
        $user->availability = $request->availability;
        $user->bio = $request->bio;
        $user->save();

        return view('show_profile.setup2');
    }

    public function setup3(Request $request) {
        $request->validate([
            'skill.*' => 'string',
            'skill_level.*' => 'in:1,2,3',
        ]);

        $user = Auth::user();

        $user->skills()->detach();
        foreach ($request->skill as $i => $skill_name) {
            if ($skill_name) {
                $skill = Skill::where('name', $skill_name)->firstOr(function() use ($skill_name) {
                    $skill = new Skill();
                    $skill->name = $skill_name;
                    $skill->save();
                    return $skill;
                });
                $user->skills()->save($skill, ['level' => $request->skill_level[$i]]);
            }
        }

        return view('show_profile.setup3');
    }

    public function setup4(Request $request) {
        $request->validate([
            'twitter' => 'string|nullable',
            'github' => 'string|nullable',
            'linkedin' => 'string|nullable',
            'website' => 'string|nullable',
        ]);

        $user = Auth::user();
        $user->twitter = $request->twitter;
        $user->github = $request->github;
        $user->linkedin = $request->linkedin;
        $user->website = $request->website;
        $user->completed_initial_wizard = true;
        $user->save();

        return redirect('/home')->with('wizard', 'project');
    }
}
