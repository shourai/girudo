<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Project;
use App\User;
use App\Skill;
use App\ProjectMessage;
use App\ProjectCategory;
use App\UserMessage;
use App\Notifications\NewProjectMessage;
use App\Notifications\NewProjectMemberInvitationMessage;
use App\Group;

use Illuminate\Support\Facades\URL;
use Log;

class ProjectController extends Controller {
    public function new() {
        $categories = ProjectCategory::all();
        return view('project.new', ['categories' => $categories]);
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'string|nullable',
            'budget_hourly' => 'numeric|nullable|min:1000|required_without:budget_total',
            'budget_total' => 'numeric|nullable|required_without:budget_hourly',
            'project_length' => 'required|in:0,1,2,3,4',
            'project_status' => 'required|in:1,2',
            'skill.*' => 'string|nullable',
            'category' => 'required|exists:project_categories,id',
            'group' => 'nullable|exists:groups,id',
            'privacy' => 'required|in:1,2',
        ]);
        $validator->sometimes('group', 'required', function($input) {
            return $input->privacy == 2;
        });
        if ($validator->fails()) {
            return redirect()->route('new_project')->withErrors($validator)->withInput();
        }

        $project = Project::create([
            'name' => $request->name,
            'description' => $request->description ? $request->description : '',
            'budget_hourly' => $request->budget_hourly,
            'budget_total' => $request->budget_total,
            'project_length' => $request->project_length,
            'project_status' => $request->project_status,
            'owner_id' => Auth::id(),
            'category_id' => $request->category,
            'privacy' => $request->privacy,
        ]);

        if ($request->skill) {
            foreach ($request->skill as $skill_name) {
                if ($skill_name) {
                    $skill = Skill::where('name', $skill_name)->firstOr(function() use ($skill_name) {
                        $skill = new Skill();
                        $skill->name = $skill_name;
                        $skill->save();
                        return $skill;
                    });
                    $project->skills()->save($skill);
                }
            }
        }

        if ($request->group) {
            $group = Group::findOrFail($request->group);
            if (!$group->isAdmin(Auth::id())) {
                return abort(404);
            }
            $group->projects()->save($project);
        }

        $user = Auth::user();
        if (!$user->completed_initial_project) {
            $user->completed_initial_project = true;
            $user->save();
        }

        return redirect('home')->with('success', 'プロジェクトを作りました！');
    }

    public function show(Request $request, $project_id) {
        $project = Project::findOrFail($project_id);

        if ($request->has('initiator_id') && !Auth::check()) {
            // If attempting to view message and not logged in, prompt to login
            return redirect('/login');
        }

        if ($project->privacy == 2) {
            if ($project->group->privacy == 2) {
                // Private project in private group
                if (!Auth::check() || $project->group->members()->where('user_id', Auth::id())->count() == 0) {
                    return abort(403);
                }
            }
        }

        $project_messages = [];
        $messages = [];
        $initiator_id = null;

        if ($project->owner_id == Auth::id()) {
            // If project owner, show all messages on this project
            $latest_project_messages = DB::table('project_messages')
                ->select('project_id', 'initiator_id', DB::raw('MAX(created_at) as last_message_date'))
                ->where('project_id', $project_id)
                ->groupBy('project_id', 'initiator_id');
            $project_messages = ProjectMessage::joinSub($latest_project_messages, 'latest_messages', function ($join) {
                $join->on([
                    ['project_messages.project_id', '=', 'latest_messages.project_id'],
                    ['project_messages.initiator_id', '=', 'latest_messages.initiator_id'],
                    ['project_messages.created_at', '=', 'latest_messages.last_message_date'],
                ]);
            })->get();

            if ($request->has('initiator_id')) {
                $messages = $project->messages()->where('initiator_id', $request->query('initiator_id'))->orderBy('created_at', 'asc')->get();
                $initiator_id = $request->query('initiator_id');
            } else if (count($project_messages) > 0) {
                // Open the latest message
                $latest = $project_messages[count($project_messages)-1];
                $messages = $project->messages()->where('initiator_id', $latest->initiator_id)->orderBy('created_at', 'asc')->get();
                $initiator_id = $latest->initiator_id;
            }
        } else if (Auth::check()) {
            // Otherwise, get messages with the project owner
            $messages = $project->messages()->where('initiator_id', Auth::id())->orderBy('created_at', 'asc')->get();
            $initiator_id = Auth::id();
        }

        return view('project.show', ['project' => $project, 'messages' => $messages, 'project_messages' => $project_messages, 'initiator_id' => $initiator_id]);
    }

    public function edit($project_id) {
        $project = Project::findOrFail($project_id);
        if (!$project->owner_id == Auth::id()) {
            return abort(404);
        }

        $categories = ProjectCategory::all();
        return view('project.edit', ['project' => $project, 'categories' => $categories]);
    }

    public function addMember(Request $request, $project_id) {
        $project = Project::findOrFail($project_id);
        if (!$project->owner_id == Auth::id()) {
            return abort(404);
        }

        $request->validate([
            'member' => 'required',
        ]);

        $numberResults = User::where('email', $request->member)
            ->orWhere('name', 'like', '%'.$request->member.'%')
            ->count();

        if ($numberResults > 1) {
            return redirect()->route('project', ['project_id' => $project_id])->with('error', '検索結果が複数人に該当しました');
        } elseif ($numberResults == 0) {
            return redirect()->route('project', ['project_id' => $project_id])->with('error', '検索結果該当がありませんでした');
        }

        $member = User::where('email', $request->member)
            ->orWhere('name', 'like', '%'.$request->member.'%')
            ->first();

        // create url
        $url = URL::temporarySignedRoute('project_invitation', now()->addHours(24), ['user_id'=>$member->id,'project_id' => $project->id]);

        if ($numberResults == 1) {
            $member->notify(new NewProjectMemberInvitationMessage($member->id,$project,$url));
        }

        return redirect()->route('project', ['project_id' => $project_id])->with('success', '招待メールを送信しました。');
    }

    public function deleteMember(Request $request, $project_id) {
        $project = Project::findOrFail($project_id);
        if (!$project->owner_id == Auth::id()) {
            return abort(404);
        }

        $request->validate([
            'member_id' => 'required',
        ]);
        $project->members()->detach($request->member_id);

        return redirect()->route('project', ['project_id' => $project_id])->with('success', 'メンバーから削除しました');
    }

    public function projectInvitation(Request $request, $project_id)
    {
        if (!$request->hasValidSignature()) {
            abort(401);
        }
        $member = User::where('email', $request->user_id)
            ->orWhere('id', $request->user_id)
            ->first();

        $project = Project::findOrFail($request->project_id);
        if (!$project->owner_id == Auth::id()) {
            return abort(404);
        }
        $project->members()->save($member);

        return redirect()->route('project', ['project_id' => $project_id])->with('success', 'メンバーに追加されました。');
    }

    public function update(Request $request, $project_id) {
        $project = Project::findOrFail($project_id);
        if (!$project->owner_id == Auth::id()) {
            return abort(404);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'string|nullable',
            'budget_hourly' => 'numeric|nullable|min:1000|required_without:budget_total',
            'budget_total' => 'numeric|nullable|required_without:budget_hourly',
            'project_length' => 'required|in:0,1,2,3,4',
            'project_status' => 'required|in:1,2',
            'skill.*' => 'string|nullable',
            'category' => 'required|exists:project_categories,id',
            'group' => 'nullable|exists:groups,id',
            'privacy' => 'required|in:1,2',
        ]);
        $validator->sometimes('group', 'required', function($input) {
            return $input->privacy == 2;
        });
        if ($validator->fails()) {
            return redirect()->route('edit_project', ['project_id' => $project_id])->withErrors($validator)->withInput();
        }

        $project->name = $request->name;
        $project->description = $request->description;
        $project->budget_hourly = $request->budget_hourly;
        $project->budget_total = $request->budget_total;
        $project->project_length = $request->project_length;
        $project->project_status = $request->project_status;
        $project->category_id = $request->category;
        $project->privacy = $request->privacy;
        $project->save();

        $skills = [];
        if ($request->skill) {
            foreach ($request->skill as $skill_name) {
                if ($skill_name) {
                    $skill = Skill::where('name', $skill_name)->firstOr(function() use ($skill_name) {
                        $skill = new Skill();
                        $skill->name = $skill_name;
                        $skill->save();
                        return $skill;
                    });
                    $skills[] = $skill->id;
                }
            }
        }

        $project->skills()->sync($skills);

        if ($request->group) {
            $group = Group::findOrFail($request->group);
            if (!$group->isAdmin(Auth::id())) {
                return abort(404);
            }
            $group->projects()->save($project);
        } elseif ($project->group) {
            $project->group_id = null;
            $project->save();
        }

        return redirect()->route('project', ['project_id' => $project->id]);
    }

    public function sendMessage(Request $request, $project_id, $initiator_id) {
        $project = Project::findOrFail($project_id);
        if (!$project->owner_id == Auth::id() && !$initiator_id == Auth::id()) {
            return abort(404);
        }
        if ($project->owner_id == $initiator_id) {
            return abort(404);
        }

        $request->validate([
            'message' => 'string',
        ]);

        ProjectMessage::create([
            'project_id' => $project_id,
            'send_id' => Auth::id(),
            'message' => $request->message,
            'initiator_id' => $initiator_id,
        ]);

        if (Auth::id() == $project->owner_id) {
            $user = User::findOrFail($initiator_id);
        } else {
            $user = User::findOrFail($project->owner_id);
        }
        if ($user->notification_project_message) {
            $user->notify(new NewProjectMessage($project_id, $initiator_id, $request->message));
        }

        return redirect()->back();
    }

    public function delete(Request $request, $project_id) {
        $project = Project::findOrFail($project_id);
        if (!$project->owner_id == Auth::id()) {
            return abort(404);
        }

        $project->delete();
        return redirect('home');
    }

    public function setup1() {
        $categories = ProjectCategory::all();
        return view('project.setup1', ['categories' => $categories]);
    }

    public function setup2View(Request $request) {
        return view('project.setup2', [
            'name' => $request->name,
            'description' => $request->description,
            'category' => $request->category,
            'skill' => $request->skill,
        ]);
    }

    public function setup3View(Request $request) {
        return view('project.setup3', [
            'name' => $request->name,
            'description' => $request->description,
            'category' => $request->category,
            'skill' => $request->skill,
        ]);
    }

    public function setup1Post(Request $request) {
        $categories = ProjectCategory::all();
        return view('project.setup1', [
            'categories' => $categories,
            'name' => $request->name,
            'description' => $request->description,
            'category' => $request->category,
            'skill' => $request->skill,
        ]);
    }

    public function setup2(Request $request) {
        $request->validate([
            'name' => 'required',
            'description' => 'string|nullable',
            'category' => 'required|exists:project_categories,id',
        ]);

        return view('project.setup2', [
            'name' => $request->name,
            'description' => $request->description,
            'category' => $request->category,
            'skill' => $request->skill,
        ]);
    }

    public function setup3(Request $request) {
        $request->validate([
            'name' => 'required',
            'description' => 'string|nullable',
            'category' => 'required|exists:project_categories,id',
            'skill.*' => 'string|nullable',
        ]);

        return view('project.setup3', [
            'name' => $request->name,
            'description' => $request->description,
            'category' => $request->category,
            'skill' => $request->skill,
        ]);
    }

    public function setup4(Request $request) {
        $request->validate([
            'name' => 'required',
            'description' => 'string|nullable',
            'category' => 'required|exists:project_categories,id',
            'skill.*' => 'string|nullable',
            'budget_hourly' => 'numeric|nullable|min:1000|required_without:budget_total',
            'budget_total' => 'numeric|nullable|required_without:budget_hourly',
            'project_length' => 'required|in:0,1,2,3,4',
            'project_status' => 'required|in:1,2',
        ]);

        $project = Project::create([
            'name' => $request->name,
            'description' => $request->description ? $request->description : '',
            'budget_hourly' => $request->budget_hourly,
            'budget_total' => $request->budget_total,
            'project_length' => $request->project_length,
            'project_status' => $request->project_status,
            'owner_id' => Auth::id(),
            'category_id' => $request->category,
        ]);

        if ($request->skill) {
            foreach ($request->skill as $skill_name) {
                if ($skill_name) {
                    $skill = Skill::where('name', $skill_name)->firstOr(function() use ($skill_name) {
                        $skill = new Skill();
                        $skill->name = $skill_name;
                        $skill->save();
                        return $skill;
                    });
                    $project->skills()->save($skill);
                }
            }
        }

        $user = Auth::user();
        $user->completed_initial_project = true;
        $user->save();

        return redirect('home')->with('wizard', 'complete');
    }
}
