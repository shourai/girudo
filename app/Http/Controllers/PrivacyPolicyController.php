<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Log;

class PrivacyPolicyController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | RuleController Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // use RegistersUsers;
    //
    // /**
    //  * Where to redirect users after registration.
    //  *
    //  * @var string
    //  */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    public function index()
    {
      return view('privacy_policy.index');
    }


}
