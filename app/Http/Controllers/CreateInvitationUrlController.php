<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Log;


class CreateInvitationUrlController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | UnableRegisterController Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // use RegistersUsers;
    //
    // /**
    //  * Where to redirect users after registration.
    //  *
    //  * @var string
    //  */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    public function index()
    {
      return view('create_invitation_url.index', ['create_url' => '']);
    }

    public function create()
    {
      // create url
      $url = URL::temporarySignedRoute('create_invitation_url.invitation_register', now()->addHours(24), []);

      return view('create_invitation_url.index', ['create_url' => $url]);

    }

    public function invitationRegister(Request $request)
    {
      if (!$request->hasValidSignature()) {
          abort(401);
      }

      return redirect('register');
    }




}
