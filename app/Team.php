<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model {
    protected $table = 'teams';

    protected $fillable = ['owner_id'];

    public function teamMembers() {
        return $this->hasMany('App\User');
    }

    public function owner() {
        return $this->hasOne('App\User', 'owner_id');
    }
}
