<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\HtmlString;
use League\CommonMark\CommonMarkConverter;

class User extends Authenticatable implements MustVerifyEmail {
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'bio', 'twitter', 'github',
        'linkedin', 'website', 'profile_pic', 'availability',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function team() {
        return $this->belongsTo('App\Team', 'team_id');
    }

    public function ownTeam() {
        return $this->hasOne('App\Team', 'owner_id');
    }

    public function groups() {
        return $this->belongsToMany('App\Group');
    }

    public function skills() {
        return $this->belongsToMany('App\Skill')->using('App\SkillUser')->withPivot(['level']);
    }

    public function extraProjects() {
        return $this->hasMany('App\ExtraProject');
    }

    public function hasShownWizard() {
        $this->shown_initial_wizard = true;
        $this->save();
    }

    public function projects() {
        return $this->hasMany('App\Project', 'owner_id');
    }

    public function participatingProjects() {
        return $this->belongsToMany('App\Project', 'project_members');
    }

    public function currentProjects() {
        $own = $this->projects()->where('project_status', 1);
        $participate = $this->participatingProjects()->select('projects.*')->where('project_status', 1);

        return $own->union($participate);
    }

    public function pastProjects() {
        $own = $this->projects()->where('project_status', 2);
        $participate = $this->participatingProjects()->select('projects.*')->where('project_status', 2);

        return $own->union($participate);
    }

    public function getBio() {
        $converter = new CommonMarkConverter([
            'allow_unsafe_links' => false,
            'html_input' => 'escape',
        ]);
        return new HtmlString($converter->convertToHtml($this->bio));
    }
}
