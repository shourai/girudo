<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMessage extends Model {
    protected $fillable = ['project_id', 'send_id', 'message', 'initiator_id'];

    public function author() {
        return $this->belongsTo('App\User', 'send_id');
    }

    public function initiator() {
        return $this->belongsTo('App\User', 'initiator_id');
    }

    public function project() {
        return $this->belongsTo('App\Project', 'project_id');
    }
}
