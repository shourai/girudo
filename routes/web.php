<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

Auth::routes(['verify' => true]);

Route::get('/unable_register', 'UnableRegisterController@index')->name('unable_register');
Route::get('/create_invitation_url', 'CreateInvitationUrlController@index')->name('create_invitation_url.index');
Route::post('/create_invitation_url/create', 'CreateInvitationUrlController@create')->name('create_invitation_url.create');
Route::get('/create_invitation_url/invitation_register', 'CreateInvitationUrlController@invitationRegister')->middleware('signed')->name('create_invitation_url.invitation_register');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/byebye', 'HomeController@byebye')->name('byebye');
Route::get('/users/{user_id}', 'ShowProfile@index')->name('profile');
Route::get('/users/{user_id}/edit', 'ShowProfile@edit')->name('edit_profile');
Route::post('/users/{user_id}/edit', 'ShowProfile@save');
Route::post('/users/{user_id}/skills', 'ShowProfile@addSkill')->name('profile_add_skill');
Route::post('/users/{user_id}/extra_project', 'ShowProfile@addExtraProject')->name('profile_add_extra_project');
Route::get('/users/{user_id}/delete_confirm', 'ShowProfile@deleteConfirm');
Route::post('/users/{user_id}/delete', 'ShowProfile@delete');
Route::get('/users/{user_id}/add_member', 'ShowProfile@addMember')->name('profile_add_member');
Route::post('/users/{user_id}/add_member', 'ShowProfile@saveMember');
Route::get('/new_project', 'ProjectController@new')->name('new_project');
Route::post('/new_project', 'ProjectController@create');
Route::get('/projects/{project_id}', 'ProjectController@show')->name('project');
Route::get('/project/{project_id}/edit', 'ProjectController@edit')->name('edit_project');
Route::post('/project/{project_id}/edit', 'ProjectController@update');
Route::post('/project/{project_id}/delete', 'ProjectController@delete')->name('delete_project');
Route::get('/projects/{project_id}/message/{initiator_id}', 'ProjectController@message')->name('project_message');
Route::post('/projects/{project_id}/message/{initiator_id}', 'ProjectController@sendMessage');
Route::post('/project/{project_id}/add_member', 'ProjectController@addMember')->name('add_project_member');
Route::post('/project/{project_id}/delete_member', 'ProjectController@deleteMember')->name('delete_project_member');
Route::get('/users/{user_id}/message/{initiator_id}', 'ShowProfile@message')->name('user_message');
Route::post('/users/{user_id}/message/{initiator_id}', 'ShowProfile@sendMessage');
Route::get('/settings', 'HomeController@settings')->name('settings');
Route::post('/settings', 'HomeController@saveSettings');
Route::get('/messages', 'HomeController@messages')->name('messages');
Route::get('/byebye', 'HomeController@byebye')->name('byebye');
Route::get('/users/{user_id}', 'ShowProfile@index')->name('profile');
Route::get('/projects/{project_id}', 'ProjectController@show')->name('project');
Route::get('/project/{project_id}/project_invitation', 'ProjectController@projectInvitation')->middleware('signed')->name('project_invitation');
Route::get('/groups', 'GroupController@list')->name('groups');
Route::get('/groups/{group_id}', 'GroupController@show')->name('group');
Route::get('/groups/{group_id}/invitation', 'GroupController@invitation')->middleware('signed')->name('group_invitation');

Route::get('/rule', 'RuleController@index')->name('rule');
Route::get('/privacy_policy', 'PrivacyPolicyController@index')->name('privacy_policy');

Route::get('/clear-cache', function() {$exitCode = Artisan::call('cache:clear');return "Cache is cleared";});

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/freelancers', 'HomeController@freelancers')->name('freelancers');
    Route::get('/users/{user_id}/edit', 'ShowProfile@edit')->name('edit_profile');
    Route::post('/users/{user_id}/edit', 'ShowProfile@save');
    Route::post('/users/{user_id}/skills', 'ShowProfile@addSkill')->name('profile_add_skill');
    Route::post('/users/{user_id}/extra_project', 'ShowProfile@addExtraProject')->name('profile_add_extra_project');
    Route::post('/users/{user_id}/skills/delete', 'ShowProfile@deleteSkill')->name('profile_delete_skill');
    Route::post('/users/{user_id}/new_group', 'ShowProfile@newGroup')->name('profile_new_group');
    Route::get('/users/{user_id}/delete_confirm', 'ShowProfile@deleteConfirm');
    Route::post('/users/{user_id}/delete', 'ShowProfile@delete');
    Route::get('/users/{user_id}/add_member', 'ShowProfile@addMember')->name('profile_add_member');
    Route::post('/users/{user_id}/add_member', 'ShowProfile@saveMember');
    Route::get('/new_project', 'ProjectController@new')->name('new_project');
    Route::post('/new_project', 'ProjectController@create');
    Route::get('/project/{project_id}/edit', 'ProjectController@edit')->name('edit_project');
    Route::post('/project/{project_id}/edit', 'ProjectController@update');
    Route::post('/project/{project_id}/delete', 'ProjectController@delete')->name('delete_project');
    Route::post('/projects/{project_id}/message/{initiator_id}', 'ProjectController@sendMessage')->name('project_message');
    Route::post('/project/{project_id}/add_member', 'ProjectController@addMember')->name('add_project_member');
    Route::post('/project/{project_id}/delete_member', 'ProjectController@deleteMember')->name('delete_project_member');
    Route::get('/users/{user_id}/message/{initiator_id}', 'ShowProfile@message')->name('user_message');
    Route::post('/users/{user_id}/message/{initiator_id}', 'ShowProfile@sendMessage');
    Route::get('/settings', 'HomeController@settings')->name('settings');
    Route::post('/settings', 'HomeController@saveSettings');
    Route::get('/messages', 'HomeController@messages')->name('messages');
    Route::get('/new_group', 'GroupController@new')->name('group_new');
    Route::post('/new_group', 'GroupController@create');
    Route::get('/groups/{group_id}/edit', 'GroupController@edit')->name('group_edit');
    Route::post('/groups/{group_id}/edit', 'GroupController@update');
    Route::post('/groups/{group_id}/delete', 'GroupController@delete')->name('group_delete');
    Route::post('/groups/{group_id}/add_member', 'GroupController@addMember')->name('add_group_member');
    Route::post('/groups/{group_id}/delete_member', 'GroupController@deleteMember')->name('delete_group_member');
    Route::post('/group_exit/{user_id}', 'GroupController@exitUser')->name('group_exit');
    Route::get('/profile_setup/1', 'ShowProfile@setup1')->name('profile_setup_start');
    Route::post('/profile_setup/2', 'ShowProfile@setup2')->name('profile_setup_step2');
    Route::get('/profile_setup/2', 'ShowProfile@setup2View');
    Route::post('/profile_setup/3', 'ShowProfile@setup3')->name('profile_setup_step3');
    Route::get('/profile_setup/3', 'ShowProfile@setup3View');
    Route::post('/profile_setup/4', 'ShowProfile@setup4')->name('profile_setup_finish');
    Route::get('/project_setup/1', 'ProjectController@setup1')->name('project_setup_start');
    Route::post('/project_setup/1', 'ProjectController@setup1Post');
    Route::post('/project_setup/2', 'ProjectController@setup2')->name('project_setup_step2');
    Route::get('/project_setup/2', 'ProjectController@setup2View');
    Route::post('/project_setup/3', 'ProjectController@setup3')->name('project_setup_step3');
    Route::get('/project_setup/3', 'ProjectController@setup3View');
    Route::post('/project_setup/4', 'ProjectController@setup4')->name('project_setup_finish');
});
